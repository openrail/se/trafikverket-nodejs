# How to Contribute

When contributing to this repository, please first discuss the change you wish to make via raising an [new issue](https://gitlab.com/openrail/se/trafikverket-nodejs/issues/new).

## Tooling

This repository makes use of several development tools to automate certain parts of the development process. All of these should be inlcuded as a devDependency within the `package.json` so a simple `npm i` should install them all for development. This however wont install any extensions required on your IDE so that is up to you to solve!.

Some of the tools we use include:
* [eslint](https://eslint.org/) for linting
* [semantic-release](https://www.npmjs.com/package/semantic-release) for auto verisoning
* [jest](https://jestjs.io/) for testing
* [typescript](https://www.typescriptlang.org/) for strict typing

## Coding

All code should be written using TypeScript following the current ESLint rules within the repository. Any public APIs should have full typings made available through an exported interface as to allow full integration with end users projects.

## Versioning

This repository makes use of automatic versioning during the CI/CD pipeline using [semantic-release](https://www.npmjs.com/package/semantic-release). Because of this no contributor needs to manually update any version numbers regarding package deployment, however, it does require contributors to use specific git commit message prefixes. 

## Commit messages

Due to using [semantic-release](https://www.npmjs.com/package/semantic-release) git commits need to follow the angular commit message format. Full details can be found at [Angular Git Commit Guidlines](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines) but for a short run down:

```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```
The **header** of the commit message is mandatory.
**scope** is optional within the header, if used it can relate to anything specifying place of the commit change e.g. `TrainAnnouncement`, `TrainStation`, ect...

**body** and **footer** are both optional and should include extra information about the commit.

**type** can be any of the following:
* **feat**: A new feature
* **fix**: A bug fix
* **docs**: Documentation only changes
* **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
* **refactor**: A code change that neither fixes a bug nor adds a feature
* **perf**: A code change that improves performance
* **test**: Adding missing or correcting existing tests
* **chore**: Changes to the build process or auxiliary tools and libraries such as documentation generation 

### Commit Message Hook

There is a commit message hook available which will do basic checking on the git commit messages. This is up to the developer whether they wish to use it or not but it is reccomended to use.

The hook can be found on the [Commit Message Hook Snippet](https://gitlab.com/openrail/se/trafikverket-nodejs/snippets/1918490) and should be saved at `.git/hooks/commit-msg` within the repository and made executable. 

## Merge Request Process

Update the README.md if any changes need documenting or existing documentation needs updating.

If a new feature is added it is a good idea to add examples to the examples folder for CommonJS, ES6 Modules, and TypeScript variants.

All commits should follow the [commit message](#commit-messages) guidelines.

You may merge the request in once you have the approval of at least 1 maintainer.

