import 'jest';
import * as indexTS from './index';

import { TrafikverketAPI } from './lib/trafikverketAPI';
import { DataField, Filter, GeoMode, GeoShape } from './lib/filter';

import { TrainAnnouncementQuery } from './lib/queries/trainAnnouncement';
import { TrainMessageQuery } from './lib/queries/trainMessage';
import { TrainStationQuery } from './lib/queries/trainStation';
import { ReasonCodeQuery } from './lib/queries/reasonCode';

import { TrainAnnouncement } from './lib/models/trainAnnouncement';
import { TrainMessage } from './lib/models/trainMessage';
import { TrainStation } from './lib/models/trainStation';
import { ReasonCode } from './lib/models/reasonCode';

import { County } from './lib/helpers/county';

describe('index.ts', () => {
  describe('exports', () => {
    test('TrafikverketAPI', () => {
      expect(indexTS.TrafikverketAPI).toBeDefined();
      expect(indexTS.TrafikverketAPI).toEqual(TrafikverketAPI);
    });

    test('Filter', () => {
      expect(indexTS.Filter).toBeDefined();
      expect(indexTS.Filter).toEqual(Filter);
    });

    test('DataField', () => {
      expect(indexTS.DataField).toBeDefined();
      expect(indexTS.DataField).toEqual(DataField);
    });

    test('GeoMode', () => {
      expect(indexTS.GeoMode).toBeDefined();
      expect(indexTS.GeoMode).toEqual(GeoMode);
    });

    test('GeoShape', () => {
      expect(indexTS.GeoShape).toBeDefined();
      expect(indexTS.GeoShape).toEqual(GeoShape);
    });

    test('TrainAnnoucementQuery', () => {
      expect(indexTS.TrainAnnouncementQuery).toBeDefined();
      expect(indexTS.TrainAnnouncementQuery).toEqual(TrainAnnouncementQuery);
    });

    test('TrainAnnouncement', () => {
      expect(indexTS.TrainAnnouncement).toBeDefined();
      expect(indexTS.TrainAnnouncement).toEqual(TrainAnnouncement);
    });

    test('TrainMessageQuery', () => {
      expect(indexTS.TrainMessageQuery).toBeDefined();
      expect(indexTS.TrainMessageQuery).toEqual(TrainMessageQuery);
    });

    test('TrainMessage', () => {
      expect(indexTS.TrainMessage).toBeDefined();
      expect(indexTS.TrainMessage).toEqual(TrainMessage);
    });

    test('TrainStationQuery', () => {
      expect(indexTS.TrainStationQuery).toBeDefined();
      expect(indexTS.TrainStationQuery).toEqual(TrainStationQuery);
    });

    test('TrainStation', () => {
      expect(indexTS.TrainStation).toBeDefined();
      expect(indexTS.TrainStation).toEqual(TrainStation);
    });

    test('ReasonCodeQuery', () => {
      expect(indexTS.ReasonCodeQuery).toBeDefined();
      expect(indexTS.ReasonCodeQuery).toEqual(ReasonCodeQuery);
    });

    test('ReasonCode', () => {
      expect(indexTS.ReasonCode).toBeDefined();
      expect(indexTS.ReasonCode).toEqual(ReasonCode);
    });

    test('County', () => {
      expect(indexTS.County).toBeDefined();
      expect(indexTS.County).toEqual(County);
    });
  });
});
