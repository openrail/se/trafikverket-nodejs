export type SchemaSwitch<
  BaseSchema,
  ActiveSchema extends BaseSchema,
  CurrentSchema extends BaseSchema,
  ActiveType,
  DefaultType
> = CurrentSchema extends ActiveSchema ? ActiveType : DefaultType;

export function isActiveSchema<BaseSchema, ActiveSchema extends BaseSchema, CurrentSchema extends BaseSchema>
(activeSchemas: ActiveSchema[], currentSchema: CurrentSchema): boolean {
  return ((activeSchemas as BaseSchema[]).includes(currentSchema));
}
