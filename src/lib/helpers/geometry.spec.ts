import 'jest';
import 'jest-extended';
import * as geometryTS from './geometry';

import { Point } from '../models/point';

describe('helpers/geometry.ts', () => {
  describe('exports', () => {
    test('parseGeometry', () => {
      expect(geometryTS.parseGeometry).toBeDefined();
    });
  });

  describe('parseGeometry', () => {
    test('should parse from RawGeometry to Geometry', () => {
      const mockData: geometryTS.RawGeometry = {
        SWEREF99TM: 'POINT (1 2)',
        WGS84: 'POINT (3 4)',
      };

      const result: geometryTS.Geometry = geometryTS.parseGeometry(mockData);

      expect(result).toBeDefined();
      expect(result.SWEREF99TM).toBeInstanceOf(Point);
      expect(result.SWEREF99TM.easting).toBe(1);
      expect(result.SWEREF99TM.northing).toBe(2);
      expect(result.WGS84).toBeInstanceOf(Point);
      expect(result.WGS84.latitude).toBe(3);
      expect(result.WGS84.longitude).toBe(4);
    });
  });
});
