import { Point, PointType } from '../models/point';

export interface Geometry {
  SWEREF99TM: Point<PointType.SWEREF99TM> | null;
  WGS84: Point<PointType.WGS84> | null;
}

export interface RawGeometry {
  SWEREF99TM: string;
  WGS84: string;
};

export function parseGeometry(rawInput: RawGeometry): Geometry {
  return {
    SWEREF99TM: (rawInput.SWEREF99TM) ? Point.SWEREF99TM(rawInput.SWEREF99TM) : null,
    WGS84: (rawInput.WGS84) ? Point.WGS84(rawInput.WGS84) : null,
  };
}
