export interface Location {
  name: string;
  order: number;
  priority: number;
}

export interface RawLocation {
  LocationName: string;
  Order: number;
  Priority: number;
}

export function parseLocation(rawInput: RawLocation): Location {
  return {
    name: rawInput.LocationName,
    order: rawInput.Order,
    priority: rawInput.Priority,
  };
}
