export interface CodeDescription {
  code: string;
  description: string;
}

export interface RawCodeDescription {
  Code: string;
  Description: string;
}

export function parseCodeDescription(rawInput: RawCodeDescription): CodeDescription {
  return {
    code: rawInput.Code,
    description: rawInput.Description,
  };
}
