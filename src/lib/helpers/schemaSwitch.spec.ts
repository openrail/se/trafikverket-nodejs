import 'jest';
import 'jest-extended';
import * as schemaSwitchTS from './schemaSwitch';

type BaseSchemas = '1' | '1.1' | '1.2';

const activeSchemas: BaseSchemas[] = ['1.1', '1.2'];

describe('helpers/schemaSwitch.ts', () => {
  describe('exports', () => {
    test('isActiveSchema', () => {
      expect(schemaSwitchTS.isActiveSchema).toBeDefined();
    });
  });

  describe('isActiveSchema', () => {
    test('should return true if schema is in activeList', () => {
      const result: boolean = schemaSwitchTS.isActiveSchema(activeSchemas, '1.1');

      expect(result).toBeTrue();
    });

    test('should return false if schema is not in activeList', () => {
      const result: boolean = schemaSwitchTS.isActiveSchema(activeSchemas, '1');

      expect(result).toBeFalse();
    });
  });
});
