import 'jest';
import 'jest-extended';
import * as locationTS from './location';

describe('helpers/location.ts', () => {
  describe('exports', () => {
    test('parseLocation', () => {
      expect(locationTS.parseLocation).toBeDefined();
    });
  });

  describe('parseLocation', () => {
    test('should parse from RawLocation to Location', () => {
      const mockData: locationTS.RawLocation = {
        LocationName: 'nameTest',
        Order: 1,
        Priority: 2
      };

      const result: locationTS.Location = locationTS.parseLocation(mockData);

      expect(result).toBeDefined();
      expect(result.name).toBeString();
      expect(result.name).toEqual(mockData.LocationName);
      expect(result.order).toBeNumber();
      expect(result.order).toEqual(mockData.Order);
      expect(result.priority).toBeNumber();
      expect(result.priority).toEqual(mockData.Priority);
    });
  });
});
