export interface TrafficImpact {
  affectedLocation: string[];
  fromLocation: string[];
  toLocation: string[];
}

export interface RawTrafficImpact {
  AffectedLocation: string[];
  FromLocation: string[];
  ToLocation: string[];
}

export function parseTrafficImpact(rawInput: RawTrafficImpact): TrafficImpact {
  return {
    affectedLocation: rawInput.AffectedLocation,
    toLocation: rawInput.ToLocation,
    fromLocation: rawInput.FromLocation,
  };
}
