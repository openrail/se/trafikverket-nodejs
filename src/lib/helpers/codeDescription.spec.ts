import 'jest';
import 'jest-extended';
import * as codeDescriptionTS from './codeDescription';

describe('helpers/codeDescription.ts', () => {
  describe('exports', () => {
    test('parseCodeDescription', () => {
      expect(codeDescriptionTS.parseCodeDescription).toBeDefined();
    });
  });

  describe('parseCodeDescription', () => {
    test('should parse from RawCodeDescription to CodeDescription', () => {
      const mockData: codeDescriptionTS.RawCodeDescription = {
        Code: 'codeTest',
        Description: 'descriptionTest'
      };

      const result: any = codeDescriptionTS.parseCodeDescription(mockData);

      expect(result).toBeDefined();
      expect(result.code).toBeString();
      expect(result.code).toEqual(mockData.Code);
      expect(result.description).toBeString();
      expect(result.description).toEqual(mockData.Description);
    });
  });
});
