import 'jest';
import 'jest-extended';
import * as trafficImpactTS from './trafficImpact';

describe('helpers/trafficImpact.ts', () => {
  describe('exports', () => {
    test('parseTrafficImpact', () => {
      expect(trafficImpactTS.parseTrafficImpact).toBeDefined();
    });
  });

  describe('parseTrafficImpact', () => {
    test('should parse from RawTrafficImpact to TrafficImpact', () => {
      const mockData: trafficImpactTS.RawTrafficImpact = {
        AffectedLocation: ['affectedTest'],
        FromLocation: ['fromTest'],
        ToLocation: ['toTest']
      };

      const result: trafficImpactTS.TrafficImpact = trafficImpactTS.parseTrafficImpact(mockData);

      expect(result).toBeDefined();
      expect(result.affectedLocation).toBeArray();
      expect(result.affectedLocation).toEqual(['affectedTest']);
      expect(result.fromLocation).toBeArray();
      expect(result.fromLocation).toEqual(['fromTest']);
      expect(result.toLocation).toBeArray();
      expect(result.toLocation).toEqual(['toTest']);
    });
  });
});
