jest.mock('http');
jest.mock('https');

import { request as httpRequest } from 'http';
import { request as httpsRequest } from 'https';
import { Readable } from 'stream';

import 'jest';
import 'jest-extended';
import * as trafikverketAPITS from './trafikverketAPI';
import { URL } from 'url';

type TestInstance = any;

describe('trafikverketAPI.ts', () => {
  describe('exports', () => {
    test('TrafikverketAPI', () => {
      expect(trafikverketAPITS.TrafikverketAPI).toBeDefined();
    });
  });

  describe('TrafikverketAPI', () => {
    describe('constructor', () => {
      test('constructs with default URI', () => {
        const instance: TestInstance = new trafikverketAPITS.TrafikverketAPI('testAPI');

        expect(instance).toBeInstanceOf(trafikverketAPITS.TrafikverketAPI);
        expect(instance.uri).toBeInstanceOf(URL);
        expect(instance.uri.href).toEqual('https://api.trafikinfo.trafikverket.se/v2');
      });

      test('constructs with custom uri', () => {
        const instance: TestInstance = new trafikverketAPITS.TrafikverketAPI('testAPI', {
          host: 'http://example.com/path',
        });

        expect(instance).toBeInstanceOf(trafikverketAPITS.TrafikverketAPI);
        expect(instance.uri).toBeInstanceOf(URL);
        expect(instance.uri.href).toEqual('http://example.com/path/v2');
      });

      test('constructs with custom path', () => {
        const instance: TestInstance = new trafikverketAPITS.TrafikverketAPI('testAPI', { path: '/v32' });

        expect(instance).toBeInstanceOf(trafikverketAPITS.TrafikverketAPI);
        expect(instance.uri).toBeInstanceOf(URL);
        expect(instance.uri.href).toEqual('https://api.trafikinfo.trafikverket.se/v32');
      });

      test('constructs with http library', () => {
        const instance: TestInstance = new trafikverketAPITS.TrafikverketAPI('testAPI', {
          host: 'http://example.com',
        });

        expect(instance.requestLib).toBeDefined();
        expect(instance.requestLib).toEqual(httpRequest);
      });

      test('constructs with http library', () => {
        const instance: TestInstance = new trafikverketAPITS.TrafikverketAPI('testAPI', {
          host: 'https://example.com',
        });

        expect(instance.requestLib).toBeDefined();
        expect(instance.requestLib).toEqual(httpsRequest);
      });
    });

    describe('getRequestOptions', () => {
      it('should return request options', () => {
        const instance: TestInstance = new trafikverketAPITS.TrafikverketAPI('testAPI', {
          host: 'http://example.com:8080',
          path: '/v32',
        });

        const result = instance.getRequestOptions(123);

        expect(result).toBeObject();
        expect(result.protocol).toEqual('http:');
        expect(result.host).toEqual('example.com');
        expect(result.port).toEqual('8080');
        expect(result.path).toEqual('/v32/data.json');
        expect(result.method).toEqual('POST');
        expect(result.headers).toBeObject();
        expect(result.headers['content-type']).toEqual('text/xml');
        expect(result.headers['content-length']).toEqual('123');
        expect(result.headers['accept']).toEqual('application/json');
        expect(result.headers['accept-encoding']).toEqual('gzip');
      });
    });

    describe('getRequestBody', () => {
      it('should return query wrapped in xml', () => {
        const instance: TestInstance = new trafikverketAPITS.TrafikverketAPI('testAPI');

        const result = instance.getRequestBody('test');

        expect(result).toStartWith('<REQUEST>');
        expect(result).toEndWith('</REQUEST>');
        expect(result).toContain('test');
      });

      it('should return query with LOGIN tag', () => {
        const instance: TestInstance = new trafikverketAPITS.TrafikverketAPI('testAPI');

        const result = instance.getRequestBody('test');

        expect(result).toContain('<LOGIN authenticationkey="testAPI" />');
      });
    });

    // describe('parseResponse', () => {
    //   it('should return a function', () => {
    //     const result = (trafikverketAPITS.TrafikverketAPI as TestInstance).parseResponse('', 0, () => {}, () => {});

    //     expect(result).toBeInstanceOf(Function);
    //   });

    //   it('should reject when invalid json response is sent', (done) => {
    //     const rejectMock = (arg: any) => {
    //       expect(arg).toBeInstanceOf(Error);
    //       done();
    //     };

    //     const result = (trafikverketAPITS.TrafikverketAPI as TestInstance).parseResponse('', 0, () => {}, rejectMock);

    //     result();
    //   });

    //   it('should reject when non API error json response is sent', (done) => {
    //     const rejectMock = (arg: any) => {
    //       expect(arg).toBeInstanceOf(Error);
    //       done();
    //     };

    //     const mockData = JSON.stringify({ test: true });
    //     const result = (trafikverketAPITS.TrafikverketAPI as TestInstance).parseResponse(mockData, 0, () => {}, rejectMock);

    //     result();
    //   });

    //   it('should reject when API error json response is sent', (done) => {
    //     const rejectMock = (arg: any) => {
    //       expect(arg).toBeInstanceOf(Error);
    //       expect(arg.message).toEqual('test'),
    //       done();
    //     };

    //     const mockData = JSON.stringify({
    //       RESPONSE: {
    //         RESULT: [{
    //           ERROR: {
    //             MESSAGE: 'test',
    //             SOURCE: 'test2'
    //           }
    //         }]
    //       }
    //     });
    //     const result = (trafikverketAPITS.TrafikverketAPI as TestInstance).parseResponse(mockData, 0, () => {}, rejectMock);

    //     result();
    //   });

    //   it('should reject when status code 200 and non RESPONSE is returned', (done) => {
    //     const rejectMock = (arg: any) => {
    //       expect(arg).toBeInstanceOf(Error);
    //       done();
    //     };

    //     const mockData = JSON.stringify({});
    //     const result = (trafikverketAPITS.TrafikverketAPI as TestInstance).parseResponse(mockData, 200, () => {}, rejectMock);

    //     result();
    //   });

    //   it('should resolve with first result', (done) => {
    //     const resolveMock = (arg: any) => {
    //       expect(arg).toBeObject();
    //       expect(arg.test).toEqual(true);
    //       done();
    //     };

    //     const mockData = JSON.stringify({
    //       RESPONSE: {
    //         RESULT: [{
    //           test: true,
    //         }],
    //       }
    //     });
    //     const result = (trafikverketAPITS.TrafikverketAPI as TestInstance).parseResponse(mockData, 200, resolveMock, () => {});

    //     result();
    //   });
    // });

    describe('handleResponse', () => {
      it('should return a function', () => {
        const result = (trafikverketAPITS.TrafikverketAPI as TestInstance).handleResponse(() => {}, () => {});

        expect(result).toBeInstanceOf(Function);
      });

      it('should reject if gunzip errors', (done) => {
        const rejectMock = (arg: any) => {
          expect(arg).toBeDefined();
          done();
        };
        const stream = new Readable();
        stream.on('error', () => {});
        stream.push('chunk');

        const result = (trafikverketAPITS.TrafikverketAPI as TestInstance).handleResponse(() => {}, rejectMock);

        result(stream);

        stream.destroy(new Error('test error'));
      });

    });

    describe('request', () => {
    }); 
  });
});
