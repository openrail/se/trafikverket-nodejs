import { Country, County } from './helpers/county';

enum BaseFilter {
  Equal = 'EQ',
  Exists = 'EXISTS',
  GreaterThan = 'GT',
  GreaterThanOrEqual = 'GTE',
  LessThan = 'LT',
  LessThanOrEqual = 'LTE',
  NotEqual = 'NE',
  Like = 'LIKE',
  NotLike = 'NOTLIKE',
  In = 'IN',
  NotIn = 'NOTIN',
}

enum GeoFilter {
  Within = 'WITHIN',
  Intersects = 'INTERSECTS',
  Near = 'NEAR',
}

enum ModifierFilter {
  ElementMatch = 'ELEMENTMATCH',
  Not = 'NOT',
  And = 'AND',
  Or = 'OR',
}

export type FilterType = BaseFilter | GeoFilter | ModifierFilter;

export enum DataField {
  ActivityId = 'ActivityId',
  ActivityType = 'ActivityType',
  Advertised = 'Advertised',
  AdvertisedTimeAtLocation = 'AdvertisedTimeAtLocation',
  AdvertisedTrainIdent = 'AdvertisedTrainIdent',
  Booking = 'Booking',
  Code = 'Code',
  Cancled = 'Cancled',
  Deleted = 'Deleted',
  Deviation = 'Deviation',
  EstimatedTimeAtLocation = 'EstimatedTimeAtLocation',
  EstimatedTimeIsPreliminary = 'EstimatedTimeIsPreliminary',
  FromLocation = 'FromLocation',
  GroupDescription = 'GroupDescription',
  LocationSignature = 'LocationSignature',
  Level1Description = 'Level1Description',
  Level2Description = 'Level2Description',
  Level3Description = 'Level3Description',
  MobileWebLink = 'MobileWebLink',
  ModifiedTime = 'ModifiedTime',
  NewEquipment = 'NewEquipment',
  Operator = 'Operator',
  OtherInformation = 'OtherInformation',
  PlannedEstimatedTimeAtLocation = 'PlannedEstimatedTimeAtLocation',
  PlannedEstimatedTimeAtLocationIsValid = 'PlannedEstimatedTimeAtLocationIsValid',
  ProductInformation = 'ProductInformation',
  ScheduledDepartureDateTime = 'ScheduledDepartureDateTime',
  Service = 'Service',
  TechnicalDateTime = 'TechnicalDateTime',
  TechnicalTrainIdent = 'TechnicalTrainIdent',
  TimeAtLocation = 'TimeAtLocation',
  TimeAtLocationWithSeconds = 'TimeAtLocationWithSeconds',
  ToLocation = 'ToLocation',
  TrackAtLocation = 'TrackAtLocation',
  TrainComposition = 'TrainComposition',
  TrainOwner = 'TrainOwner',
  TypeOfTraffic = 'TypeOfTraffic',
  WebLink = 'WebLink',
  WebLinkName = 'WebLinkName',
  ViaFromLocation = 'ViaFromLocation',
  ViaToLocation = 'ViaToLocation',
  AffectedLocation = 'AffectedLocation',
  CountyNumber = 'CountyNo',
  EndDateTime = 'EndDateTime',
  EventId = 'EventId',
  ExpectTrafficImpact = 'ExpectTrafficImpact',
  ExternalDescription = 'ExternalDescription',
  Geometry = 'Geometry',
  Header = 'Header',
  LastUpdateDateTime = 'LastUpdateDateTime',
  PrognosticatedEndDateTimeTrafficImpact = 'PrognosticatedEndDateTimeTrafficImpact',
  ReasonCodeText = 'ReasonCodeText',
  StartDateTime = 'StartDateTime',
  TrafficImpact = 'TrafficImpact',
  AdvertisedLocationName = 'AdvertisedLocationName',
  AdvertisedShortLocationName = 'AdvertisedShortLocationName',
  CountryCode = 'CountryCode',
  LocationInformationText = 'LocationInformationText',
  PlatformLine = 'PlatformLine',
  Prognosticated = 'Prognosticated',
}

type DataFields = DataField | keyof typeof DataField;

export enum GeoMode {
  SWEREF99TM = 'Geometry.SWEREF99TM',
}

export enum GeoShape {
  Box = 'box',
  Polygon = 'polygon',
  Center = 'center',
}

type GeoModes = GeoMode | keyof typeof GeoMode;
type GeoShapes = GeoShape | keyof typeof GeoShape;

type DataValue<T extends DataFields> =
  T extends DataField.CountyNumber | 'CountyNumber' ? (County | number) :
  T extends DataField.CountryCode | 'CountryCode' ? (Country | string) :
  string;

const baseFilterValues: BaseFilter[] = Object.values(BaseFilter);
const modifierFilterValues: ModifierFilter[] = Object.values(ModifierFilter);

export class Filter<T extends FilterType> {
  private type: FilterType;
  private name: string;
  private shape: string;
  private value: string;
  private radius: string;
  private minDistance: string;
  private maxDistance: string;

  private subFilters: Filter<any>[];

  constructor(type: BaseFilter, name: string, value: string);
  constructor(type: GeoFilter.Within, name: string, shape: GeoShape.Center, value: string, radius: string);
  constructor(type: GeoFilter.Within, name: string, shape: GeoShape.Box | GeoShape.Polygon, value: string);
  constructor(type: GeoFilter.Intersects, name: string, shape: GeoShape.Center, value: string, radius: string);
  constructor(type: GeoFilter.Intersects, name: string, shape: GeoShape.Box | GeoShape.Polygon, value: string);
  constructor(type: GeoFilter.Near, name: string, value: string, minDistance: string, maxDistance: string);
  constructor(type: ModifierFilter, ...filters: Array<Filter<FilterType>>);
  constructor(type: T, ...options: Array<string | Filter<FilterType>>) {
    this.type = type;

    if (baseFilterValues.includes(type as BaseFilter)) {
      this.name = options[0] as string;
      this.value = options[1] as string;
    } else if (type === GeoFilter.Within || type === GeoFilter.Intersects) {
      this.name = options[0] as string;
      this.shape = options[1] as string;
      this.value = options[2] as string;
      this.radius = options[3] as string;
    } else if (type === GeoFilter.Near) {
      this.name = options[0] as string;
      this.value = options[1] as string;
      this.minDistance = options[2] as string;
      this.maxDistance = options[3] as string;
    } else if (modifierFilterValues.includes(type as ModifierFilter)) {
      this.subFilters = options as Array<Filter<FilterType>>;
    } else {
      throw new Error(`Filter cannot be constructed with type: '${type}'. Not supported`);
    }
  }

  public toString(): string {
    if (baseFilterValues.includes(this.type as BaseFilter)) {
      return `<${this.type} name="${this.name}" value="${this.value}" />`;
    }

    if (this.type === GeoFilter.Within || this.type === GeoFilter.Intersects) {
      return (this.radius)
        ? `<${this.type} name="${this.name}" shape="${this.shape}" value="${this.value}" radius="${this.radius}" />`
        : `<${this.type} name="${this.name}" shape="${this.shape}" value="${this.value}" />`;
    }

    if (this.type === GeoFilter.Near) {
      return `<${this.type} name="${this.name}" value="${this.value}" mindistance="${this.minDistance}" maxdistance="${this.maxDistance}" />`;
    }

    if (this.type as ModifierFilter) {
      return `<${this.type}>
                ${this.subFilters.reduce((finalFilter, filter) => `${finalFilter}${filter.toString()}`, '')}
              </${this.type}>`;
    }

    throw new Error(`Filter cannot be stringified with type: '${this.type}'. Not supported`);
  }

  public static equal<T extends DataFields>(dataField: T, value: DataValue<T>): Filter<BaseFilter.Equal>;
  public static equal<T extends string>(name: T, value: string | number): Filter<BaseFilter.Equal> {
    return new Filter<BaseFilter.Equal>(BaseFilter.Equal, name, `${value}`);
  }

  public static exists<T extends DataFields>(dataField: T, exists: boolean): Filter<BaseFilter.Exists>;
  public static exists<T extends string>(name: T, exists: boolean): Filter<BaseFilter.Exists> {
    return new Filter<BaseFilter.Exists>(BaseFilter.Exists, name, (exists === true) ? 'true' : 'false');
  }

  public static greaterThan<T extends DataFields>(dataField: T, above: string | number): Filter<BaseFilter.GreaterThan>;
  public static greaterThan<T extends string>(name: T, value: string | number): Filter<BaseFilter.GreaterThan> {
    return new Filter<BaseFilter.GreaterThan>(BaseFilter.GreaterThan, name, `${value}`);
  }

  public static greaterThanOrEqual<T extends DataFields>(dataField: T, above: string | number): Filter<BaseFilter.GreaterThanOrEqual>;
  public static greaterThanOrEqual<T extends string>(name: T, value: string | number): Filter<BaseFilter.GreaterThanOrEqual> {
    return new Filter<BaseFilter.GreaterThanOrEqual>(BaseFilter.GreaterThanOrEqual, name, `${value}`);
  }

  public static lessThan<T extends DataFields>(dataField: T, above: string | number): Filter<BaseFilter.LessThan>;
  public static lessThan<T extends string>(name: T, value: string | number): Filter<BaseFilter.LessThan> {
    return new Filter<BaseFilter.LessThan>(BaseFilter.LessThan, name, `${value}`);
  }

  public static lessThanOrEqual<T extends DataFields>(dataField: T, above: string | number): Filter<BaseFilter.LessThanOrEqual>;
  public static lessThanOrEqual<T extends string>(name: T, value: string | number): Filter<BaseFilter.LessThanOrEqual> {
    return new Filter<BaseFilter.LessThanOrEqual>(BaseFilter.LessThanOrEqual, name, `${value}`);
  }

  public static notEqual<T extends DataFields>(dataField: T, value: DataValue<T>): Filter<BaseFilter.NotEqual>;
  public static notEqual<T extends string>(name: T, value: string | number): Filter<BaseFilter.NotEqual> {
    return new Filter<BaseFilter.NotEqual>(BaseFilter.NotEqual, name, `${value}`);
  }

  public static like<T extends DataFields>(dataField: T, value: RegExp): Filter<BaseFilter.Like>;
  public static like<T extends DataFields>(dataField: T, value: string): Filter<BaseFilter.Like>;
  public static like<T extends string>(name: T, value: string | RegExp): Filter<BaseFilter.Like> {
    const regex: RegExp = (value instanceof RegExp) ? value : new RegExp(value);

    if (regex.flags !== '') {
      throw new Error('Regex cannot contain any flags');
    }

    return new Filter<BaseFilter.Like>(BaseFilter.Like, name, regex.toString());
  }

  public static notLike<T extends DataFields>(dataField: T, value: RegExp): Filter<BaseFilter.NotLike>;
  public static notLike<T extends DataFields>(dataField: T, value: string): Filter<BaseFilter.NotLike>;
  public static notLike<T extends string>(name: T, value: string | RegExp): Filter<BaseFilter.NotLike> {
    const regex: RegExp = (value instanceof RegExp) ? value : new RegExp(value);

    if (regex.flags !== '') {
      throw new Error('Regex cannot contain any flags');
    }

    return new Filter<BaseFilter.NotLike>(BaseFilter.NotLike, name, regex.toString());
  }

  public static in<T extends DataFields>(dataField: T, value: DataValue<T>): Filter<BaseFilter.In>;
  public static in<T extends string>(name: T, value: string | number): Filter<BaseFilter.In> {
    return new Filter<BaseFilter.In>(BaseFilter.In, name, `${value}`);
  }

  public static notIn<T extends DataFields>(dataField: T, value: DataValue<T>): Filter<BaseFilter.NotIn>;
  public static notIn<T extends string>(name: T, value: string | number): Filter<BaseFilter.NotIn> {
    return new Filter<BaseFilter.NotIn>(BaseFilter.NotIn, name, `${value}`);
  }

  public static within<T extends GeoModes>(dataField: T, shape: GeoShape.Center, value: string, radius: string | number): Filter<GeoFilter.Within>;
  public static within<T extends GeoModes>(dataField: T, shape: GeoShape.Box | GeoShape.Polygon, value: string): Filter<GeoFilter.Within>;
  public static within<T extends string>(name: T, shape: string, value: string, radius?: string | number): Filter<GeoFilter.Within> {
    return (shape === GeoShape.Center)
      ? new Filter<GeoFilter.Within>(GeoFilter.Within, name, GeoShape.Center, value, `${radius}`)
      : new Filter<GeoFilter.Within>(GeoFilter.Within, name, shape as GeoShape.Box | GeoShape.Polygon, value);
  }

  public static intersects<T extends GeoModes>(dataField: T, shape: GeoShape.Center, value: string, radius: string | number): Filter<GeoFilter.Intersects>;
  public static intersects<T extends GeoModes>(dataField: T, shape: GeoShape.Box | GeoShape.Polygon, value: string): Filter<GeoFilter.Intersects>;
  public static intersects<T extends string>(name: T, shape: string, value: string, radius?: string | number): Filter<GeoFilter.Intersects> {
    return (shape === GeoShape.Center)
      ? new Filter<GeoFilter.Intersects>(GeoFilter.Intersects, name, GeoShape.Center, value, `${radius}`)
      : new Filter<GeoFilter.Intersects>(GeoFilter.Intersects, name, shape as GeoShape.Box | GeoShape.Polygon, value);
  }

  public static near<T extends GeoModes>(dataField: T, value: string, minDistance: string | number, maxDistance: string | number): Filter<GeoFilter.Near>;
  public static near<T extends string>(name: T, value: string, minDistance: string | number, maxDistance: string | number): Filter<GeoFilter.Near> {
    return new Filter<GeoFilter.Near>(GeoFilter.Near, name, value, `${minDistance}`, `${maxDistance}`);
  }

  public static not(...filters: Array<Filter<FilterType>>): Filter<ModifierFilter.Not> {
    return new Filter<ModifierFilter.Not>(ModifierFilter.Not, ...filters);
  }

  public static and(...filters: Array<Filter<FilterType>>): Filter<ModifierFilter.And> {
    return new Filter<ModifierFilter.And>(ModifierFilter.And, ...filters);
  }

  public static or(...filters: Array<Filter<FilterType>>): Filter<ModifierFilter.Or> {
    return new Filter<ModifierFilter.Or>(ModifierFilter.Or, ...filters);
  }

  public static elementMatch(...filters: Array<Filter<FilterType>>): Filter<ModifierFilter.ElementMatch> {
    return new Filter<ModifierFilter.ElementMatch>(ModifierFilter.ElementMatch, ...filters);
  }
}
