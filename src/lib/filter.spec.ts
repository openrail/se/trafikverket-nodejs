import 'jest';
import 'jest-extended';
import * as filterTS from './filter';

type TestInstance = any;

describe('filter.ts', () => {
  describe('exports', () => {
    test('DataField', () => {
      expect(filterTS.DataField).toBeDefined();
    });

    test('GeoMode', () => {
      expect(filterTS.GeoMode).toBeDefined();
    });

    test('GeoShape', () => {
      expect(filterTS.GeoShape).toBeDefined();
    });

    test('Filter', () => {
      expect(filterTS.Filter).toBeDefined();
    });
  });

  describe('DataField', () => {
    test('DataField should be an enum', () => {
      expect(filterTS.DataField).toBeInstanceOf(Object);
    });
    test('All values should be strings', () => {
      const keys: string[] = Object.keys(filterTS.DataField);

      keys.forEach((key: string): void => {
        expect(typeof (filterTS.DataField as { [K: string]: string })[key]).toEqual('string');
      });
    });
  });

  describe('GeoMode', () => {
    test('GeoMode should be an enum', () => {
      expect(filterTS.GeoMode).toBeInstanceOf(Object);
    });

    test('All values should be strings', () => {
      const keys: string[] = Object.keys(filterTS.GeoMode);

      keys.forEach((key: string): void => {
        expect(typeof (filterTS.GeoMode as { [K: string]: string })[key]).toEqual('string');
      });
    });

    test('SWEREF99TM should equal "Geometry.SWEREF99TM"', () => {
      expect(filterTS.GeoMode.SWEREF99TM).toEqual('Geometry.SWEREF99TM');
    });
  });

  describe('GeoShape', () => {
    test('GeoShape should be an enum', () => {
      expect(filterTS.GeoShape).toBeInstanceOf(Object);
    });

    test('All values should be strings', () => {
      const keys: string[] = Object.keys(filterTS.GeoShape);

      keys.forEach((key: string): void => {
        expect(typeof (filterTS.GeoShape as { [K: string]: string })[key]).toEqual('string');
      });
    });

    test('Box should equal "box"', () => {
      expect(filterTS.GeoShape.Box).toEqual('box');
    });

    test('Polygon should equal "polygon"', () => {
      expect(filterTS.GeoShape.Polygon).toEqual('polygon');
    });

    test('Center should equal "center"', () => {
      expect(filterTS.GeoShape.Center).toEqual('center');
    });
  });


  describe('Filter', () => {
    describe('constructor', () => {
      test('should throw error if created with invalid type', () => {
        expect(() => {
          new (filterTS.Filter as any)('invalid', 'test', 'test');
        }).toThrowError();
      });
    });

    describe('toString', () => {
      test('should throw error if created with invalid type', () => {
        const test = new (filterTS.Filter as any)('EQ', 'test', 'test');
        test.type = 'FAKE';
        expect(() => {
          test.toString();
        }).toThrowError();
      });
    });

    describe('equal', () => {
      test('should be a static function', () => {
        expect(filterTS.Filter.equal).toBeInstanceOf(Function);
      });

      test('should create an instance of Filter', () => {
        const instance: TestInstance = filterTS.Filter.equal('ActivityId', 'test');

        expect(instance).toBeInstanceOf(filterTS.Filter);
      });

      test('Filter instance should stringify into xml entry', () => {
        const instance: TestInstance = filterTS.Filter.equal('ActivityId', 'test');

        expect(instance.toString()).toEqual('<EQ name="ActivityId" value="test" />');
      });
    });

    describe('exists', () => {
      test('should be a static function', () => {
        expect(filterTS.Filter.exists).toBeInstanceOf(Function);
      });

      test('should create an instance of Filter', () => {
        const instance: TestInstance = filterTS.Filter.exists('ActivityId', true);

        expect(instance).toBeInstanceOf(filterTS.Filter);
      });

      test('Filter instance should stringify into true xml entry', () => {
        const instance: TestInstance = filterTS.Filter.exists('ActivityId', true);

        expect(instance.toString()).toEqual('<EXISTS name="ActivityId" value="true" />');
      });

      test('Filter instance should stringify into false xml entry', () => {
        const instance: TestInstance = filterTS.Filter.exists('ActivityId', false);

        expect(instance.toString()).toEqual('<EXISTS name="ActivityId" value="false" />');
      });
    });

    describe('greaterThan', () => {
      test('should be a static function', () => {
        expect(filterTS.Filter.greaterThan).toBeInstanceOf(Function);
      });

      test('should create an instance of Filter', () => {
        const instance: TestInstance = filterTS.Filter.greaterThan('ActivityId', 12);

        expect(instance).toBeInstanceOf(filterTS.Filter);
      });

      test('Filter instance should stringify into string xml entry', () => {
        const instance: TestInstance = filterTS.Filter.greaterThan('ActivityId', '12');

        expect(instance.toString()).toEqual('<GT name="ActivityId" value="12" />');
      });

      test('Filter instance should stringify into number xml entry', () => {
        const instance: TestInstance = filterTS.Filter.greaterThan('ActivityId', 13);

        expect(instance.toString()).toEqual('<GT name="ActivityId" value="13" />');
      });
    });

    describe('greaterThanOrEqual', () => {
      test('should be a static function', () => {
        expect(filterTS.Filter.greaterThanOrEqual).toBeInstanceOf(Function);
      });

      test('should create an instance of Filter', () => {
        const instance: TestInstance = filterTS.Filter.greaterThanOrEqual('ActivityId', 12);

        expect(instance).toBeInstanceOf(filterTS.Filter);
      });

      test('Filter instance should stringify into string xml entry', () => {
        const instance: TestInstance = filterTS.Filter.greaterThanOrEqual('ActivityId', '12');

        expect(instance.toString()).toEqual('<GTE name="ActivityId" value="12" />');
      });

      test('Filter instance should stringify into number xml entry', () => {
        const instance: TestInstance = filterTS.Filter.greaterThanOrEqual('ActivityId', 13);

        expect(instance.toString()).toEqual('<GTE name="ActivityId" value="13" />');
      });
    });

    describe('lessThan', () => {
      test('should be a static function', () => {
        expect(filterTS.Filter.lessThan).toBeInstanceOf(Function);
      });

      test('should create an instance of Filter', () => {
        const instance: TestInstance = filterTS.Filter.lessThan('ActivityId', 12);

        expect(instance).toBeInstanceOf(filterTS.Filter);
      });

      test('Filter instance should stringify into string xml entry', () => {
        const instance: TestInstance = filterTS.Filter.lessThan('ActivityId', '12');

        expect(instance.toString()).toEqual('<LT name="ActivityId" value="12" />');
      });

      test('Filter instance should stringify into number xml entry', () => {
        const instance: TestInstance = filterTS.Filter.lessThan('ActivityId', 13);

        expect(instance.toString()).toEqual('<LT name="ActivityId" value="13" />');
      });
    });

    describe('lessThanOrEqual', () => {
      test('should be a static function', () => {
        expect(filterTS.Filter.lessThanOrEqual).toBeInstanceOf(Function);
      });

      test('should create an instance of Filter', () => {
        const instance: TestInstance = filterTS.Filter.lessThanOrEqual('ActivityId', 12);

        expect(instance).toBeInstanceOf(filterTS.Filter);
      });

      test('Filter instance should stringify into string xml entry', () => {
        const instance: TestInstance = filterTS.Filter.lessThanOrEqual('ActivityId', '12');

        expect(instance.toString()).toEqual('<LTE name="ActivityId" value="12" />');
      });

      test('Filter instance should stringify into number xml entry', () => {
        const instance: TestInstance = filterTS.Filter.lessThanOrEqual('ActivityId', 13);

        expect(instance.toString()).toEqual('<LTE name="ActivityId" value="13" />');
      });
    });

    describe('notEqual', () => {
      test('should be a static function', () => {
        expect(filterTS.Filter.notEqual).toBeInstanceOf(Function);
      });

      test('should create an instance of Filter', () => {
        const instance: TestInstance = filterTS.Filter.notEqual('ActivityId', 'test');

        expect(instance).toBeInstanceOf(filterTS.Filter);
      });

      test('Filter instance should stringify into xml entry', () => {
        const instance: TestInstance = filterTS.Filter.notEqual('ActivityId', 'test');

        expect(instance.toString()).toEqual('<NE name="ActivityId" value="test" />');
      });
    });

    describe('like', () => {
      test('should be a static function', () => {
        expect(filterTS.Filter.like).toBeInstanceOf(Function);
      });

      test('should create an instance of Filter', () => {
        const instance: TestInstance = filterTS.Filter.like('ActivityId', 'test');

        expect(instance).toBeInstanceOf(filterTS.Filter);
      });

      test('should throw an error when using a regex with flags', () => {
        expect(() => {
          filterTS.Filter.like('ActivityId', RegExp('/test/', 'mi'));
        }).toThrowError('Regex cannot contain any flags');
      });

      test('Filter instance should stringify into string xml entry', () => {
        const instance: TestInstance = filterTS.Filter.like('ActivityId', 'test');

        expect(instance.toString()).toEqual('<LIKE name="ActivityId" value="/test/" />');
      });

      test('Filter instance should stringify into regex xml entry', () => {
        const instance: TestInstance = filterTS.Filter.like('ActivityId', RegExp('test'));

        expect(instance.toString()).toEqual('<LIKE name="ActivityId" value="/test/" />');
      });
    });

    describe('notLike', () => {
      test('should be a static function', () => {
        expect(filterTS.Filter.notLike).toBeInstanceOf(Function);
      });

      test('should create an instance of Filter', () => {
        const instance: TestInstance = filterTS.Filter.notLike('ActivityId', 'test');

        expect(instance).toBeInstanceOf(filterTS.Filter);
      });

      test('should throw an error when using a regex with flags', () => {
        expect(() => {
          filterTS.Filter.notLike('ActivityId', RegExp('/test/', 'mi'));
        }).toThrowError('Regex cannot contain any flags');
      });

      test('Filter instance should stringify into string xml entry', () => {
        const instance: TestInstance = filterTS.Filter.notLike('ActivityId', 'test');

        expect(instance.toString()).toEqual('<NOTLIKE name="ActivityId" value="/test/" />');
      });

      test('Filter instance should stringify into regex xml entry', () => {
        const instance: TestInstance = filterTS.Filter.notLike('ActivityId', RegExp('test'));

        expect(instance.toString()).toEqual('<NOTLIKE name="ActivityId" value="/test/" />');
      });
    });

    describe('in', () => {
      test('should be a static function', () => {
        expect(filterTS.Filter.in).toBeInstanceOf(Function);
      });

      test('should create an instance of Filter', () => {
        const instance: TestInstance = filterTS.Filter.in('ActivityId', 'test');

        expect(instance).toBeInstanceOf(filterTS.Filter);
      });

      test('Filter instance should stringify into xml entry', () => {
        const instance: TestInstance = filterTS.Filter.in('ActivityId', 'test');

        expect(instance.toString()).toEqual('<IN name="ActivityId" value="test" />');
      });
    });

    describe('notIn', () => {
      test('should be a static function', () => {
        expect(filterTS.Filter.notIn).toBeInstanceOf(Function);
      });

      test('should create an instance of Filter', () => {
        const instance: TestInstance = filterTS.Filter.notIn('ActivityId', 'test');

        expect(instance).toBeInstanceOf(filterTS.Filter);
      });

      test('Filter instance should stringify into xml entry', () => {
        const instance: TestInstance = filterTS.Filter.notIn('ActivityId', 'test');

        expect(instance.toString()).toEqual('<NOTIN name="ActivityId" value="test" />');
      });
    });

    describe('within', () => {
      test('should be a static function', () => {
        expect(filterTS.Filter.within).toBeInstanceOf(Function);
      });

      test('should create an instance of Filter', () => {
        const instance: TestInstance = filterTS.Filter.within('SWEREF99TM', filterTS.GeoShape.Box, '0, 0, 10, 10');

        expect(instance).toBeInstanceOf(filterTS.Filter);
      });

      test('Filter instance should stringify into xml box entry', () => {
        const instance: TestInstance = filterTS.Filter.within('SWEREF99TM', filterTS.GeoShape.Box, '0, 0, 10, 10');

        expect(instance.toString()).toEqual('<WITHIN name="SWEREF99TM" shape="box" value="0, 0, 10, 10" />');
      });

      test('Filter instance should stringify into xml polygon entry', () => {
        const instance: TestInstance = filterTS.Filter.within('SWEREF99TM', filterTS.GeoShape.Polygon, '0, 0, 10, 10');

        expect(instance.toString()).toEqual('<WITHIN name="SWEREF99TM" shape="polygon" value="0, 0, 10, 10" />');
      });

      test('Filter instance should stringify into string xml center entry', () => {
        const instance: TestInstance = filterTS.Filter.within('SWEREF99TM', filterTS.GeoShape.Center, '0, 0, 10, 10', '12');

        expect(instance.toString()).toEqual('<WITHIN name="SWEREF99TM" shape="center" value="0, 0, 10, 10" radius="12" />');
      });

      test('Filter instance should stringify into number xml center entry', () => {
        const instance: TestInstance = filterTS.Filter.within('SWEREF99TM', filterTS.GeoShape.Center, '0, 0, 10, 10', 13);

        expect(instance.toString()).toEqual('<WITHIN name="SWEREF99TM" shape="center" value="0, 0, 10, 10" radius="13" />');
      });
    });

    describe('intersects', () => {
      test('should be a static function', () => {
        expect(filterTS.Filter.intersects).toBeInstanceOf(Function);
      });

      test('should create an instance of Filter', () => {
        const instance: TestInstance = filterTS.Filter.intersects('SWEREF99TM', filterTS.GeoShape.Box, '0, 0, 10, 10');

        expect(instance).toBeInstanceOf(filterTS.Filter);
      });

      test('Filter instance should stringify into xml box entry', () => {
        const instance: TestInstance = filterTS.Filter.intersects('SWEREF99TM', filterTS.GeoShape.Box, '0, 0, 10, 10');

        expect(instance.toString()).toEqual('<INTERSECTS name="SWEREF99TM" shape="box" value="0, 0, 10, 10" />');
      });

      test('Filter instance should stringify into xml polygon entry', () => {
        const instance: TestInstance = filterTS.Filter.intersects('SWEREF99TM', filterTS.GeoShape.Polygon, '0, 0, 10, 10');

        expect(instance.toString()).toEqual('<INTERSECTS name="SWEREF99TM" shape="polygon" value="0, 0, 10, 10" />');
      });

      test('Filter instance should stringify into string string xml center entry', () => {
        const instance: TestInstance = filterTS.Filter.intersects('SWEREF99TM', filterTS.GeoShape.Center, '0, 0, 10, 10', '12');

        expect(instance.toString()).toEqual('<INTERSECTS name="SWEREF99TM" shape="center" value="0, 0, 10, 10" radius="12" />');
      });

      test('Filter instance should stringify into number xml center entry', () => {
        const instance: TestInstance = filterTS.Filter.intersects('SWEREF99TM', filterTS.GeoShape.Center, '0, 0, 10, 10', 13);

        expect(instance.toString()).toEqual('<INTERSECTS name="SWEREF99TM" shape="center" value="0, 0, 10, 10" radius="13" />');
      });
    });

    describe('near', () => {
      test('should be a static function', () => {
        expect(filterTS.Filter.near).toBeInstanceOf(Function);
      });

      test('should create an instance of Filter', () => {
        const instance: TestInstance = filterTS.Filter.near('SWEREF99TM', filterTS.GeoShape.Center, '12', '13');

        expect(instance).toBeInstanceOf(filterTS.Filter);
      });

      test('Filter instance should stringify into string xml entry', () => {
        const instance: TestInstance = filterTS.Filter.near('SWEREF99TM', filterTS.GeoShape.Center, '12', '13');

        expect(instance.toString()).toEqual('<NEAR name="SWEREF99TM" value="center" mindistance="12" maxdistance="13" />');
      });

      test('Filter instance should stringify into number xml entry', () => {
        const instance: TestInstance = filterTS.Filter.near('SWEREF99TM', filterTS.GeoShape.Center, 14, 15);

        expect(instance.toString()).toEqual('<NEAR name="SWEREF99TM" value="center" mindistance="14" maxdistance="15" />');
      });
    });

    describe('not', () => {
      test('should be a static function', () => {
        expect(filterTS.Filter.not).toBeInstanceOf(Function);
      });

      test('should create an instance of Filter', () => {
        const instance: TestInstance = filterTS.Filter.not(filterTS.Filter.equal('ActivityId', 'test'));

        expect(instance).toBeInstanceOf(filterTS.Filter);
      });

      test('Filter instance should stringify into string xml entry', () => {
        const instance: TestInstance = filterTS.Filter.not(filterTS.Filter.equal('ActivityId', 'test'));
        const xmlString: string = instance.toString();

        expect(xmlString).toStartWith('<NOT>');
        expect(xmlString).toContain('<EQ name="ActivityId" value="test" />');
        expect(xmlString).toEndWith('</NOT>');
      });
    });

    describe('and', () => {
      test('should be a static function', () => {
        expect(filterTS.Filter.and).toBeInstanceOf(Function);
      });

      test('should create an instance of Filter', () => {
        const instance: TestInstance = filterTS.Filter.and(filterTS.Filter.equal('ActivityId', 'test'));

        expect(instance).toBeInstanceOf(filterTS.Filter);
      });

      test('Filter instance should stringify into 2 element xml entry', () => {
        const instance: TestInstance = filterTS.Filter.and(filterTS.Filter.equal('ActivityId', 'test'), filterTS.Filter.equal('ActivityType', 'test2'));
        const xmlString: string = instance.toString();

        expect(xmlString).toStartWith('<AND>');
        expect(xmlString).toContain('<EQ name="ActivityId" value="test" />');
        expect(xmlString).toContain('<EQ name="ActivityType" value="test2" />');
        expect(xmlString).toEndWith('</AND>');
      });

      test('Filter instance should stringify into 3 element xml entry', () => {
        const instance: TestInstance = filterTS.Filter.and(
          filterTS.Filter.equal('ActivityId', 'test'),
          filterTS.Filter.equal('ActivityType', 'test2'),
          filterTS.Filter.equal('Advertised', 'test3')
        );
        const xmlString: string = instance.toString();

        expect(xmlString).toStartWith('<AND>');
        expect(xmlString).toContain('<EQ name="ActivityId" value="test" />');
        expect(xmlString).toContain('<EQ name="ActivityType" value="test2" />');
        expect(xmlString).toContain('<EQ name="Advertised" value="test3" />');
        expect(xmlString).toEndWith('</AND>');
      });
    });

    describe('or', () => {
      test('should be a static function', () => {
        expect(filterTS.Filter.or).toBeInstanceOf(Function);
      });

      test('should create an instance of Filter', () => {
        const instance: TestInstance = filterTS.Filter.or(filterTS.Filter.equal('ActivityId', 'test'));

        expect(instance).toBeInstanceOf(filterTS.Filter);
      });

      test('Filter instance should stringify into 2 element xml entry', () => {
        const instance: TestInstance = filterTS.Filter.or(filterTS.Filter.equal('ActivityId', 'test'), filterTS.Filter.equal('ActivityType', 'test2'));
        const xmlString: string = instance.toString();

        expect(xmlString).toStartWith('<OR>');
        expect(xmlString).toContain('<EQ name="ActivityId" value="test" />');
        expect(xmlString).toContain('<EQ name="ActivityType" value="test2" />');
        expect(xmlString).toEndWith('</OR>');
      });

      test('Filter instance should stringify into 3 element xml entry', () => {
        const instance: TestInstance = filterTS.Filter.or(
          filterTS.Filter.equal('ActivityId', 'test'),
          filterTS.Filter.equal('ActivityType', 'test2'),
          filterTS.Filter.equal('Advertised', 'test3')
        );
        const xmlString: string = instance.toString();

        expect(xmlString).toStartWith('<OR>');
        expect(xmlString).toContain('<EQ name="ActivityId" value="test" />');
        expect(xmlString).toContain('<EQ name="ActivityType" value="test2" />');
        expect(xmlString).toContain('<EQ name="Advertised" value="test3" />');
        expect(xmlString).toEndWith('</OR>');
      });
    });

    describe('elementMatch', () => {
      test('should be a static function', () => {
        expect(filterTS.Filter.elementMatch).toBeInstanceOf(Function);
      });

      test('should create an instance of Filter', () => {
        const instance: TestInstance = filterTS.Filter.elementMatch(filterTS.Filter.equal('ActivityId', 'test'));

        expect(instance).toBeInstanceOf(filterTS.Filter);
      });

      test('Filter instance should stringify into string xml entry', () => {
        const instance: TestInstance = filterTS.Filter.elementMatch(filterTS.Filter.equal('ActivityId', 'test'));
        const xmlString: string = instance.toString();

        expect(xmlString).toStartWith('<ELEMENTMATCH>');
        expect(xmlString).toContain('<EQ name="ActivityId" value="test" />');
        expect(xmlString).toEndWith('</ELEMENTMATCH>');
      });
    });
  });
});
