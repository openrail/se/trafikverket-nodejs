import 'jest';
import 'jest-extended';
import * as reasonCodeTS from './reasonCode';

import { ReasonCode } from '../models/reasonCode';

type TestInstance = any;

describe('queries/reasonCode.ts', () => {
  describe('exports', () => {
    test('ReasonCodeQuery', () => {
      expect(reasonCodeTS.ReasonCodeQuery).toBeDefined();
    });
  });

  describe('ReasonCodeQuery', () => {
    describe('constructor', () => {
      test('sets the query schema version', () => {
        const instance: TestInstance = new reasonCodeTS.ReasonCodeQuery('1');

        expect(instance.querySchema).toEqual('1');
      });

      test('should enable server side events', () => {
        const instance: TestInstance = new reasonCodeTS.ReasonCodeQuery('1', true);

        expect(instance.serverSideEvents).toEqual(true);
      });

      test('should not use server side events', () => {
        const instance: TestInstance = new reasonCodeTS.ReasonCodeQuery('1', false);

        expect(instance.serverSideEvents).toEqual(false);
      });

      test('should not use server side events by default', () => {
        const instance: TestInstance = new reasonCodeTS.ReasonCodeQuery('1');

        expect(instance.serverSideEvents).toEqual(false);
      });
    });

    describe('toString', () => {
      test('should form base xml tags correctly', () => {
        const instance: TestInstance = new reasonCodeTS.ReasonCodeQuery('1');

        const result = instance.toString();
        expect(result).toStartWith('<QUERY objecttype="ReasonCode" schemaversion="1"');
        expect(result).toEndWith('</QUERY>');
      });

      test('should include query attributes in xml tags', () => {
        const instance: TestInstance = new reasonCodeTS.ReasonCodeQuery('1');

        instance.limit(12);
        instance.skip(1);

        const result = instance.toString();
        expect(result).toStartWith('<QUERY objecttype="ReasonCode" schemaversion="1"');
        expect(result).toMatch(new RegExp('<QUERY.+?limit=\"12\".*?>', 'gm'));
        expect(result).toMatch(new RegExp('<QUERY.+?skip=\"1\".*?>', 'gm'));
        expect(result).toEndWith('</QUERY>');
      });

      test('should include filters between root xml tags', () => {
        const instance: TestInstance = new reasonCodeTS.ReasonCodeQuery('1');

        instance.filter({ toString: (): string => 'test' });

        const result = instance.toString();
        expect(result).toStartWith('<QUERY objecttype="ReasonCode" schemaversion="1"');
        expect(result).toInclude('<FILTER>test</FILTER>');
        expect(result).toEndWith('</QUERY>');
      });
    });

    describe('request', () => {
      test('calls TrafikverketAPI.request', () => {
        const instance: TestInstance = new reasonCodeTS.ReasonCodeQuery('1');

        instance.on('error', () => { /* error */ });

        const mockTrafikverketAPI = {
          request: jest.fn(() => new Promise((resolve, reject) => { reject(); }))
        };

        instance.request(mockTrafikverketAPI);

        expect(mockTrafikverketAPI.request.mock.calls.length).toBe(1);
      });

      test('calls TrafikverketAPI.request with query xml string', () => {
        const instance: TestInstance = new reasonCodeTS.ReasonCodeQuery('1');

        instance.on('error', () => { /* error */ });

        const mockTrafikverketAPI = {
          request: jest.fn((tv: any) => new Promise((resolve, reject) => { reject(); }))
        };

        instance.request(mockTrafikverketAPI);

        expect(mockTrafikverketAPI.request.mock.calls[0][0]).toEqual(instance.toString());
      });

      test('fires error event when TrafikverketAPI.request errors', (done) => {
        const instance: TestInstance = new reasonCodeTS.ReasonCodeQuery('1');

        instance.on('error', () => {
          done();
        });

        const mockTrafikverketAPI = {
          request: jest.fn((tv: any) => new Promise((resolve, reject) => { reject(); }))
        };

        instance.request(mockTrafikverketAPI);
      });

      test('fires message event when TrafikverketAPI.request returns', (done) => {
        const instance: TestInstance = new reasonCodeTS.ReasonCodeQuery('1');

        instance.on('error', () => {
          done.fail(new Error('Error event should not be fired'));
        });

        instance.on('message', (result: any) => {
          expect(result).toBeArrayOfSize(0);
          done();
        });

        const mockTrafikverketAPI = {
          request: jest.fn((tv: any) => new Promise((resolve) => { resolve({
            ReasonCode: []
          }); }))
        };

        instance.request(mockTrafikverketAPI);
      });

      test('fires message event with ReasonCode array when TrafikverketAPI.request returns', (done) => {
        const instance: TestInstance = new reasonCodeTS.ReasonCodeQuery('1');

        instance.on('error', (err: any) => {
          console.error(err);
          done.fail(new Error('Error event should not be fired'));
        });

        instance.on('message', (result: any) => {
          expect(result).toBeArrayOfSize(1);
          expect(result).toIncludeAllMembers([new ReasonCode(({} as any), '1')]);
          done();
        });

        const mockTrafikverketAPI = {
          request: jest.fn((tv: any) => new Promise((resolve) => { resolve({
            ReasonCode: [{}],
          }); }))
        };

        instance.request(mockTrafikverketAPI);
      });
    });

    describe('on', () => {
      test('returns this', () => {
        const instance: TestInstance = new reasonCodeTS.ReasonCodeQuery('1');

        const result = instance.on('test', () => {});

        expect(result).toEqual(instance);
      });
    });

    describe('once', () => {
      test('returns this', () => {
        const instance: TestInstance = new reasonCodeTS.ReasonCodeQuery('1');

        const result = instance.once('test', () => {});

        expect(result).toEqual(instance);
      });
    });
  });
});
