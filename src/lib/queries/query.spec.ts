import { URL } from 'url';

import 'jest';
import 'jest-extended';
import * as queryTS from './query';

type TestInstance = any;

class MockQuery extends queryTS.Query {
  request(): void { /* empty */ }
  toString(): string { return 'mocked'; };
};

describe('queries/query.ts', () => {
  describe('exports', () => {
    test('Query', () => {
      expect(queryTS.Query).toBeDefined();
    });
  });

  describe('Query', () => {
    describe('constructor', () => {
      test('should enable server side events', () => {
        const instance: TestInstance = new MockQuery(true);

        expect(instance.serverSideEvents).toEqual(true);
      });

      test('should not use server side events', () => {
        const instance: TestInstance = new MockQuery(false);

        expect(instance.serverSideEvents).toEqual(false);
      });

      test('should not use server side events by default', () => {
        const instance: TestInstance = new MockQuery();

        expect(instance.serverSideEvents).toEqual(false);
      });
    });

    describe('getSSEUrlFromResult', () => {
      it('should return null if result is empty', () => {
        const result = queryTS.Query.getSSEUrlFromResult();

        expect(result).toEqual(null);
      });

      it('should return URL object if result has SSEURL', () => {
        const result = queryTS.Query.getSSEUrlFromResult({
          INFO: {
            SSEURL: 'https://example.com',
          },
        });

        expect(result).toBeInstanceOf(URL);
        expect(result.href).toEqual('https://example.com/');
      });
    });

    describe('getChangeIdFromResult', () => {
      it('should return null if result is empty', () => {
        const result = queryTS.Query.getChangeIdFromResult();

        expect(result).toEqual(null);
      });

      it('should return URL object if result has SSEURL', () => {
        const result = queryTS.Query.getChangeIdFromResult({
          INFO: {
            LASTCHANGEID: 'testID',
          },
        });

        expect(result).toEqual('testID');
      });
    });

    describe('handleSSE', () => {
      it('should not set SSE listener if SSEURL is not present', () => {
        const mockReturn = (): void => {};
        const instance: TestInstance = new MockQuery(false);

        instance.handleSSE({}, mockReturn);

        expect(instance.serverSideListner).toBeUndefined();
      });
    });

    describe('handleChangeID', () => {
      it('should not set change id if LASTCHANGEID is not present', () => {
        const mockReturn = (): void => {};
        const instance: TestInstance = new MockQuery(false);

        instance.handleChangeID({}, mockReturn);

        expect(instance.queryChangeId).toEqual('0');
      });

      it('should set change id if LASTCHANGEID is present', () => {
        const mockReturn = (): void => {};
        const instance: TestInstance = new MockQuery(false);

        instance.handleChangeID({
          INFO: {
            LASTCHANGEID: '12'
          }
        }, mockReturn);

        expect(instance.queryChangeId).toEqual('12');
      });


      it('fires changeid event when LASTCHANGEID is present', (done) => {
        const mockReturn = (): void => {};
        const instance: TestInstance = new MockQuery(false);

        instance.on('error', (err: any) => {
          console.error(err);
          done.fail(new Error('Error event should not be fired'));
        });

        instance.on('changeid', (result: any) => {
          expect(result).toEqual('12');
          done();
        });

        instance.handleChangeID({
          INFO: {
            LASTCHANGEID: '12'
          }
        }, mockReturn);
      });
    });

    describe('limit', () => {
      test('should default to no limit', () => {
        const instance: TestInstance = new MockQuery(true);

        expect(instance.queryLimit).toBeUndefined();
      });

      test('should set the queries limit amount', () => {
        const instance: TestInstance = new MockQuery(true);
        instance.limit(12);

        expect(instance.queryLimit).toEqual(12);
      });

      test('should return this to allow chaining', () => {
        const instance: TestInstance = new MockQuery(true);
        const result = instance.limit(12);

        expect(result).toEqual(instance);
      });
    });

    describe('changeId', () => {
      test('should default to "0" changeId', () => {
        const instance: TestInstance = new MockQuery(true);

        expect(instance.queryChangeId).toEqual('0');
      });

      test('should set the queries change ID', () => {
        const instance: TestInstance = new MockQuery(true);
        instance.changeId('12345');

        expect(instance.queryChangeId).toEqual('12345');
      });

      test('should return this to allow chaining', () => {
        const instance: TestInstance = new MockQuery(true);
        const result = instance.changeId('12');

        expect(result).toEqual(instance);
      });
    });

    describe('skip', () => {
      test('should default to no skip', () => {
        const instance: TestInstance = new MockQuery(true);

        expect(instance.querySkip).toBeUndefined();
      });

      test('should set the queries skip amount', () => {
        const instance: TestInstance = new MockQuery(true);
        instance.skip(12);

        expect(instance.querySkip).toEqual(12);
      });

      test('should return this to allow chaining', () => {
        const instance: TestInstance = new MockQuery(true);
        const result = instance.skip(12);

        expect(result).toEqual(instance);
      });
    });

    describe('filter', () => {
      test('should default to no filters', () => {
        const instance: TestInstance = new MockQuery(true);

        expect(instance.queryFilters).toBeArrayOfSize(0);
      });

      test('should add a filter to the query', () => {
        const instance: TestInstance = new MockQuery(true);
        const mockFilter = { toString: (): string => 'test' };

        instance.filter(mockFilter);

        expect(instance.queryFilters).toBeArrayOfSize(1);
        expect(instance.queryFilters).toIncludeAllMembers([mockFilter]);
      });

      test('should add multiple filters to the query', () => {
        const instance: TestInstance = new MockQuery(true);
        const mockFilter1 = { toString: (): string => 'test1' };
        const mockFilter2 = { toString: (): string => 'test2' };

        instance.filter(mockFilter1);
        instance.filter(mockFilter2);

        expect(instance.queryFilters).toBeArrayOfSize(2);
        expect(instance.queryFilters).toIncludeAllMembers([mockFilter1, mockFilter2]);
      });

      test('should return this to allow chaining', () => {
        const instance: TestInstance = new MockQuery(true);
        const mockFilter = { toString: (): string => 'test' };
        const result = instance.filter(mockFilter);

        expect(result).toEqual(instance);
      });
    });

    describe('stringifyFilters', () => {
      test('should return an empty string if no filters present', () => {
        const instance: TestInstance = new MockQuery(true);

        expect(instance.stringifyFilters()).toEqual('');
      });

      test('should return xml with FILTER as root', () => {
        const instance: TestInstance = new MockQuery(true);

        instance.filter({ toString: () => 'test' });

        const stringifiedFilters: string = instance.stringifyFilters();

        expect(stringifiedFilters).toStartWith('<FILTER>');
        expect(stringifiedFilters).toEndWith('</FILTER>');
        expect(stringifiedFilters).toInclude('<FILTER>test</FILTER>');
      });
    });

    describe('getQueryArguments', () => {
      test('should return default attributes if no custom ones are set', () => {
        const instance: TestInstance = new MockQuery();

        expect(instance.getQueryArguments()).toInclude(' changeid="0"');
      });

      test('should return a string with sseurl attribute', () => {
        const instance: TestInstance = new MockQuery(true);

        expect(instance.getQueryArguments()).toInclude(' sseurl="true"');
      });

      test('should return a string with changeID attribute', () => {
        const instance: TestInstance = new MockQuery();

        instance.changeId('12');

        expect(instance.getQueryArguments()).toInclude(' changeid="12"');
      });

      test('should return a string with limit attribute', () => {
        const instance: TestInstance = new MockQuery();

        instance.limit(12);

        expect(instance.getQueryArguments()).toInclude(' limit="12"');
      });

      test('should return a string with skip attribute', () => {
        const instance: TestInstance = new MockQuery();

        instance.skip(12);

        expect(instance.getQueryArguments()).toInclude(' skip="12"');
      });

      test('should return with all attributes', () => {
        const instance: TestInstance = new MockQuery(true);

        instance.limit(12);
        instance.skip(12);

        expect(instance.getQueryArguments()).toInclude(' sseurl="true"');
        expect(instance.getQueryArguments()).toInclude(' limit="12"');
        expect(instance.getQueryArguments()).toInclude(' skip="12"');
      });
    });
  });
});
