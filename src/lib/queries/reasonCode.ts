import { Query } from './query';
import { URL } from 'url';

import {
  ReasonCode,
  RCSchemaVersions,
  RawReasonCodePayload,
} from '../models/reasonCode';

import { TrafikverketAPI } from '../trafikverketAPI';

import { EventSourceListner } from '../models/eventSourceListner';
import { RawResult } from '../models/response';

interface RawReasonCodeResponse<T extends RCSchemaVersions> extends RawResult {
  ReasonCode: RawReasonCodePayload<T>[];
}

export class ReasonCodeQuery<T extends RCSchemaVersions> extends Query {
  protected querySchema: T;

  constructor(schema: T, serverSideEvents?: boolean) {
    super(serverSideEvents);

    this.querySchema = schema;
  }

  private parseRawArray(dataArray: RawReasonCodePayload<T>[]): ReasonCode<T>[] {
    return dataArray
      .map((payload: RawReasonCodePayload<T>) => new ReasonCode<T>(payload, this.querySchema));
  }

  public toString(): string {
    return `<QUERY objecttype="ReasonCode" schemaversion="${this.querySchema}"${this.getQueryArguments()}>
              ${this.stringifyFilters()}
            </QUERY>`;
  }

  public request(trafikverketAPI: TrafikverketAPI): void {
    trafikverketAPI.request(this.toString())
      .then((result: RawReasonCodeResponse<T>) => {
        this.parseResponse(result);
        this.handleSSE(result, this.parseResponse);
      })
      .catch((err) => {
        this.emit('error', err);
      });
  }

  private parseResponse(data: RawResult & RawReasonCodeResponse<T>): void {
    const changeId: string = Query.getChangeIdFromResult(data);

    this.emit('changeid', changeId);
    this.emit('message', this.parseRawArray(data.ReasonCode), changeId);
  }

  public on(event: 'message', listener: (reasonCodes: ReasonCode<T>[], changeId: string) => void): this;
  public on(event: 'close', listener: () => void): this;
  public on(event: 'changeid', listener: (changeId: string) => void): this;
  public on(event: 'error', listener: (error: Error) => void): this;
  public on(event: string, listener: (...args: any[]) => void): this {
    super.on(event, listener);

    return this;
  }

  public once(event: 'message', listener: (reasonCodes: ReasonCode<T>[], changeId: string) => void): this;
  public once(event: 'close', listener: () => void): this;
  public once(event: 'changeid', listener: (changeId: string) => void): this;
  public once(event: 'error', listener: (error: Error) => void): this;
  public once(event: string, listener: (...args: any[]) => void): this {
    super.once(event, listener);

    return this;
  }
}
