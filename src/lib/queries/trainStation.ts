import { Query } from './query';

import {
  TrainStation,
  TSSchemaVersions,
  RawTrainStationPayload,
} from '../models/trainStation';

import { TrafikverketAPI } from '../trafikverketAPI';

import { RawResult } from '../models/response';

interface RawTrainStationResponse<T extends TSSchemaVersions> extends RawResult {
  TrainStation: RawTrainStationPayload<T>[];
}

export class TrainStationQuery<T extends TSSchemaVersions> extends Query {
  protected querySchema: T;

  constructor(schema: T, serverSideEvents?: boolean) {
    super(serverSideEvents);

    this.querySchema = schema;
  }

  private parseRawArray(dataArray: RawTrainStationPayload<T>[]): TrainStation<T>[] {
    return dataArray
      .map((payload: RawTrainStationPayload<T>) => new TrainStation<T>(payload, this.querySchema));
  }

  public toString(): string {
    return `<QUERY objecttype="TrainStation" schemaversion="${this.querySchema}"${this.getQueryArguments()}>
              ${this.stringifyFilters()}
            </QUERY>`;
  }

  public request(trafikverketAPI: TrafikverketAPI): void {
    trafikverketAPI.request(this.toString())
      .then((result: RawTrainStationResponse<T>) => {
        this.parseResponse(result);
        this.handleSSE(result, this.parseResponse);
      })
      .catch((err) => {
        this.emit('error', err);
      });
  }

  private parseResponse(data: RawResult & RawTrainStationResponse<T>): void {
    const changeId: string = Query.getChangeIdFromResult(data);

    this.emit('changeid', changeId);
    this.emit('message', this.parseRawArray(data.TrainStation), changeId);
  }

  public on(event: 'message', listener: (trainAnnouncements: TrainStation<T>[], changeId: string) => void): this;
  public on(event: 'close', listener: () => void): this;
  public on(event: 'changeid', listener: (changeId: string) => void): this;
  public on(event: 'error', listener: (error: Error) => void): this;
  public on(event: string, listener: (...args: any[]) => void): this {
    super.on(event, listener);

    return this;
  }

  public once(event: 'message', listener: (trainAnnouncements: TrainStation<T>[], changeId: string) => void): this;
  public once(event: 'close', listener: () => void): this;
  public once(event: 'changeid', listener: (changeId: string) => void): this;
  public once(event: 'error', listener: (error: Error) => void): this;
  public once(event: string, listener: (...args: any[]) => void): this {
    super.once(event, listener);

    return this;
  }
}
