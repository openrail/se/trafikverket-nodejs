import { Filter, FilterType } from '../filter';
import { TrafikverketAPI } from '../trafikverketAPI';
import { EventEmitter } from 'events';
import { URL } from 'url';

import { EventSourceListner } from '../models/eventSourceListner';
import { RawResult } from '../models/response';

export abstract class Query extends EventEmitter {
  protected queryLimit: number;
  protected querySkip: number;
  protected queryChangeId: string = '0';
  protected queryFilters: Filter<FilterType>[] = [];
  protected serverSideEvents: boolean;
  protected serverSideListner: EventSourceListner<any>;

  public static getSSEUrlFromResult(result: RawResult = {}): URL | null {
    return (result.INFO && result.INFO.SSEURL) ? new URL(result.INFO.SSEURL) : null;
  }

  public static getChangeIdFromResult(result: RawResult = {}): string | null {
    return (result.INFO && result.INFO.LASTCHANGEID) ? result.INFO.LASTCHANGEID : null;
  }

  constructor(serverSideEvents: boolean = false) {
    super();

    this.serverSideEvents = serverSideEvents;
  }

  protected getQueryArguments(): string {
    let queryParams: string = '';

    if (this.queryLimit) {
      queryParams += ` limit="${this.queryLimit}"`;
    }

    if (this.querySkip) {
      queryParams += ` skip="${this.querySkip}"`;
    }

    if (this.serverSideEvents) {
      queryParams += ' sseurl="true"';
    }

    if (this.queryChangeId) {
      queryParams += ` changeid="${this.queryChangeId}"`;
    }

    return queryParams;
  }

  public limit(limit: number): this {
    this.queryLimit = limit;

    return this;
  }

  public skip(skip: number): this {
    this.querySkip = skip;

    return this;
  }

  public changeId(changeId: string): this {
    this.queryChangeId = changeId;

    return this;
  }

  public filter(filter: Filter<FilterType>): this {
    this.queryFilters.push(filter);

    return this;
  }

  protected stringifyFilters(): string {
    const filters = this.queryFilters.slice(0)
      .reduce((finalFilters: string, currentFilter: Filter<FilterType>): string => {
        finalFilters += currentFilter.toString();
        return finalFilters;
      }, '');

    return (this.queryFilters.length > 0)
      ? `<FILTER>${filters}</FILTER>`
      : '';
  }

  protected handleSSE(result: RawResult, messageHandler: (data: any) => void): void {
    const sseUrl: URL = Query.getSSEUrlFromResult(result);

    if (sseUrl) {
      this.serverSideListner = new EventSourceListner(sseUrl, true);

      this.serverSideListner
        .on('message', messageHandler)
        .on('error', (err: Error): void => {
          this.emit('error', err);
        })
        .on('closed', () => {
          this.emit('closed');
          this.removeAllListeners();
        });

      this.serverSideListner.connect();
    }
  }

  protected handleChangeID(result: RawResult): void {
    const changeId: string = Query.getChangeIdFromResult(result);

    if (changeId) {
      this.queryChangeId = changeId;

      this.emit('changeid', changeId);
    }
  }

  public abstract toString(): string;
  public abstract request(trafikverketAPI: TrafikverketAPI): void;
}
