import { Query } from './query';

import {
  TrainAnnouncement,
  TASchemaVersions,
  RawTrainAnnouncementPayload,
} from '../models/trainAnnouncement';

import { TrafikverketAPI } from '../trafikverketAPI';

import { RawResult } from '../models/response';

interface RawTrainAnnouncementResponse<T extends TASchemaVersions> extends RawResult {
  TrainAnnouncement: RawTrainAnnouncementPayload<T>[];
}

export class TrainAnnouncementQuery<T extends TASchemaVersions> extends Query {
  protected querySchema: T;

  constructor(schema: T, serverSideEvents?: boolean) {
    super(serverSideEvents);

    this.querySchema = schema;
  }

  private parseRawArray(dataArray: RawTrainAnnouncementPayload<T>[]): TrainAnnouncement<T>[] {
    return dataArray
      .map((payload: RawTrainAnnouncementPayload<T>) => new TrainAnnouncement<T>(payload, this.querySchema));
  }

  public toString(): string {
    return `<QUERY objecttype="TrainAnnouncement" schemaversion="${this.querySchema}"${this.getQueryArguments()}>
              ${this.stringifyFilters()}
            </QUERY>`;
  }

  public request(trafikverketAPI: TrafikverketAPI): void {
    trafikverketAPI.request(this.toString())
      .then((result: RawTrainAnnouncementResponse<T>) => {
        this.parseResponse(result);
        this.handleSSE(result, this.parseResponse);
      })
      .catch((err) => {
        this.emit('error', err);
      });
  }

  private parseResponse(data: RawResult & RawTrainAnnouncementResponse<T>): void {
    const changeId: string = Query.getChangeIdFromResult(data);

    this.emit('changeid', changeId);
    this.emit('message', this.parseRawArray(data.TrainAnnouncement), changeId);
  }

  public on(event: 'message', listener: (trainAnnouncements: TrainAnnouncement<T>[], changeId: string) => void): this;
  public on(event: 'close', listener: () => void): this;
  public on(event: 'changeid', listener: (changeId: string) => void): this;
  public on(event: 'error', listener: (error: Error) => void): this;
  public on(event: string, listener: (...args: any[]) => void): this {
    super.on(event, listener);

    return this;
  }

  public once(event: 'message', listener: (trainAnnouncements: TrainAnnouncement<T>[], changeId: string) => void): this;
  public once(event: 'close', listener: () => void): this;
  public once(event: 'changeid', listener: (changeId: string) => void): this;
  public once(event: 'error', listener: (error: Error) => void): this;
  public once(event: string, listener: (...args: any[]) => void): this {
    super.once(event, listener);

    return this;
  }
}
