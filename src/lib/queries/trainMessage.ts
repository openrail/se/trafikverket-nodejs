import { Query } from './query';
import { URL } from 'url';

import {
  TrainMessage,
  TMSchemaVersions,
  RawTrainMessagePayload,
} from '../models/trainMessage';

import { TrafikverketAPI } from '../trafikverketAPI';

import { EventSourceListner } from '../models/eventSourceListner';
import { RawResult } from '../models/response';

interface RawTrainMessageResponse<T extends TMSchemaVersions> extends RawResult {
  TrainMessage: RawTrainMessagePayload<T>[];
}

export class TrainMessageQuery<T extends TMSchemaVersions> extends Query {
  protected querySchema: T;

  constructor(schema: T, serverSideEvents?: boolean) {
    super(serverSideEvents);

    this.querySchema = schema;
  }

  private parseRawArray(dataArray: RawTrainMessagePayload<T>[]): TrainMessage<T>[] {
    return dataArray
      .map((payload: RawTrainMessagePayload<T>) => new TrainMessage<T>(payload, this.querySchema));
  }

  public toString(): string {
    return `<QUERY objecttype="TrainMessage" schemaversion="${this.querySchema}"${this.getQueryArguments()}>
              ${this.stringifyFilters()}
            </QUERY>`;
  }

  public request(trafikverketAPI: TrafikverketAPI): void {
    trafikverketAPI.request(this.toString())
      .then((result: RawTrainMessageResponse<T>) => {
        this.parseResponse(result);
        this.handleSSE(result, this.parseResponse);
      })
      .catch((err) => {
        this.emit('error', err);
      });
  }

  private parseResponse(data: RawResult & RawTrainMessageResponse<T>): void {
    const changeId: string = Query.getChangeIdFromResult(data);

    this.emit('changeid', changeId);
    this.emit('message', this.parseRawArray(data.TrainMessage), changeId);
  }

  public on(event: 'message', listener: (trainAnnouncements: TrainMessage<T>[], changeId: string) => void): this;
  public on(event: 'close', listener: () => void): this;
  public on(event: 'changeid', listener: (changeId: string) => void): this;
  public on(event: 'error', listener: (error: Error) => void): this;
  public on(event: string, listener: (...args: any[]) => void): this {
    super.on(event, listener);

    return this;
  }

  public once(event: 'message', listener: (trainAnnouncements: TrainMessage<T>[], changeId: string) => void): this;
  public once(event: 'close', listener: () => void): this;
  public once(event: 'changeid', listener: (changeId: string) => void): this;
  public once(event: 'error', listener: (error: Error) => void): this;
  public once(event: string, listener: (...args: any[]) => void): this {
    super.once(event, listener);

    return this;
  }
}
