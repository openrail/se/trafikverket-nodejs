import { request as httpRequest, RequestOptions, ClientRequest, IncomingMessage } from 'http';
import { request as httpsRequest } from 'https';
import { URL } from 'url';
import { createGunzip } from 'zlib';

import { RawResponse, getFirstErrorResponse, RawResult } from './models/response';

enum HTTPStatusCode {
  OK = 200,
}

interface TrafikVerketAPIOptions {
  host?: string;
  path?: string;
}

type Resolve = (value?: object | PromiseLike<object>) => void;
type Reject = (reason?: any) => void;

export class TrafikverketAPI {
  private apiKey: string;
  private uri: URL;
  private requestLib: (options: RequestOptions, callback?: (res: IncomingMessage) => void) => ClientRequest;

  private static handleResponse(resolve: Resolve, reject: Reject): (res: IncomingMessage) => void {
    return (res: IncomingMessage): void => {
      const gunzip = createGunzip();    
      let result: string = '';

      res.pipe(gunzip);

      gunzip
        .on('error', reject)
        .on('data', (chunk: string): void => {
          result += chunk;
        })
        .on('end', (): void => {
          try {
            const body: RawResponse = JSON.parse(result);
    
            if (body && res.statusCode === HTTPStatusCode.OK) {
              if (body.RESPONSE && body.RESPONSE.RESULT && body.RESPONSE.RESULT[0]) {
                resolve(body.RESPONSE.RESULT[0]);
              } else {
                reject(new Error('Missing RESPONSE in body from API'));
              }
            } else {
              const apiError: RawResult = getFirstErrorResponse(body);
    
              if (!!apiError) {
                reject(new Error(apiError.ERROR.MESSAGE));
              } else {
                reject(new Error(`An error occured. StatusCode: ${res.statusCode}; Body: ${body.RESPONSE}`));
              }
            }
          } catch(err) {
            reject(err);
          }
        });
    };
  }

  constructor(apiKey: string, options: TrafikVerketAPIOptions = {}) {
    this.apiKey = apiKey;
    this.uri = new URL(`${options.host || 'https://api.trafikinfo.trafikverket.se'}${options.path || '/v2'}`);
    this.requestLib = (this.uri.protocol === 'https:') ? httpsRequest : httpRequest;
  }

  private getRequestOptions(bodyLength: number): RequestOptions {
    return {
      protocol: this.uri.protocol,
      host: this.uri.hostname,
      port: this.uri.port,
      path: `${this.uri.pathname}/data.json`,
      method: 'POST',
      headers: {
        'content-type': 'text/xml',
        'content-length': `${bodyLength}`,
        'accept': 'application/json',
        'accept-encoding': 'gzip'
      }
    };
  }

  private getRequestBody(query: string): string {
    return `
      <REQUEST>
        <LOGIN authenticationkey="${this.apiKey}" />
        ${query}
      </REQUEST>`.replace(/>\s*/g, '>').replace(/\s*</g, '<').trim();
  }

  public request(query: string): Promise<object> {
    return new Promise((resolve, reject) => {
      const requestBody: string = this.getRequestBody(query);
      const requestOptions: RequestOptions = this.getRequestOptions(Buffer.byteLength(requestBody));

      const req: ClientRequest = this.requestLib(requestOptions, TrafikverketAPI.handleResponse(resolve, reject));

      req
        .on('error', reject)
        .on('abort', () => {
          reject(new Error('Request was aborted'));
        })
        .write(Buffer.from(requestBody, 'utf8'));

      req.end();
    });
  }
}
