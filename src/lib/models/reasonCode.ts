import { Response } from './response';

export enum RCSchemaVersion {
  'v1' = '1',
}

export type RCSchemaVersions = RCSchemaVersion | '1';
export type DefaultSchemaVersion = '1';
export const defaultSchemaVersion: RCSchemaVersions & DefaultSchemaVersion = '1';

export interface RawReasonCodePayload<T> {
  Code: string;
  Deleted: boolean;
  GroupDescription: string;
  Level1Description: string;
  Level2Description: string;
  Level3Description: string;
  ModifiedTime: string;
}

export class ReasonCode<T extends RCSchemaVersions> extends Response<T> {
  public code: string;
  public deleted: boolean;
  public groupDescription: string;
  public level1Description: string;
  public level2Description: string;
  public level3Description: string;
  public modified: Date;

  constructor(payload: RawReasonCodePayload<T>, schema: T) {
    super(schema);

    this.code = payload.Code;
    this.deleted = payload.Deleted || false;
    this.groupDescription = payload.GroupDescription;
    this.level1Description = payload.Level1Description;
    this.level2Description = payload.Level2Description;
    this.level3Description = payload.Level3Description;
    this.modified = ReasonCode.parseDate(payload.ModifiedTime);
  }
}
