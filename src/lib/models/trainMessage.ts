import { Response } from './response';

import { County } from '../helpers/county';
import { RawTrafficImpact, TrafficImpact as TI, parseTrafficImpact } from '../helpers/trafficImpact';
import { SchemaSwitch, isActiveSchema } from '../helpers/schemaSwitch';
import { RawGeometry, Geometry, parseGeometry } from '../helpers/geometry';
import { CodeDescription, RawCodeDescription, parseCodeDescription } from '../helpers/codeDescription';

export enum TMSchemaVersion {
  'v1' = '1',
  'v1Dot3' = '1.3',
  'v1Dot4' = '1.4',
  'v1Dot5' = '1.5',
}

export type TMSchemaVersions = TMSchemaVersion | '1' | '1.3' | '1.4' | '1.5';
export type DefaultSchemaVersion = '1.5';
export const defaultSchemaVersion: TMSchemaVersions & DefaultSchemaVersion = '1.5';

type AboveV1Dot5 = TMSchemaVersion.v1Dot5;
type AboveV1Dot4 = TMSchemaVersion.v1Dot4 | AboveV1Dot5;
type AboveV1Dot3 = TMSchemaVersion.v1Dot3 | AboveV1Dot4;

const aboveV1Dot5: TMSchemaVersion[] = [TMSchemaVersion.v1Dot5];
const aboveV1Dot4: TMSchemaVersion[] = [TMSchemaVersion.v1Dot4, ...aboveV1Dot5];
const aboveV1Dot3: TMSchemaVersion[] = [TMSchemaVersion.v1Dot3, ...aboveV1Dot4];

type TASchemaSwitch<
  ActiveSchema extends TMSchemaVersions,
  CurrentSchema extends TMSchemaVersions,
  ActiveType,
  DefaultType
> = SchemaSwitch<TMSchemaVersions, ActiveSchema, CurrentSchema, ActiveType, DefaultType>;

type EndDateTime<T extends TMSchemaVersions, U = Date> = TASchemaSwitch<AboveV1Dot3, T, U, never>;
type ExpectTrafficImpact<T extends TMSchemaVersions, U = boolean> = TASchemaSwitch<AboveV1Dot4, T, U, never>;
type Header<T extends TMSchemaVersions, U = string> = TASchemaSwitch<AboveV1Dot4, T, U, never>;
type PrognosticatedEndDateTimeTrafficImpact<T extends TMSchemaVersions, U = Date> = TASchemaSwitch<AboveV1Dot4, T, U, never>;
type TrafficImpact<T extends TMSchemaVersions, U = TI> = TASchemaSwitch<AboveV1Dot3, T, U[], never>;
type ReasonCodeText<T extends TMSchemaVersions> = TASchemaSwitch<AboveV1Dot5, T, never, string>;
type ReasonCode<T extends TMSchemaVersions, U = CodeDescription> = TASchemaSwitch<AboveV1Dot5, T, U[], never>;

export interface RawTrainMessagePayload<T extends TMSchemaVersions> {
  AffectedLocation: string[];
  CountyNo: number[];
  Deleted: boolean;
  EndDateTime: EndDateTime<T, string>;
  EventId: string;
  ExpectTrafficImpact: ExpectTrafficImpact<T>;
  ExternalDescription: string;
  Geometry: RawGeometry;
  Header: Header<T>;
  LastUpdateDateTime: string;
  ModifiedTime: string;
  PrognosticatedEndDateTimeTrafficImpact: PrognosticatedEndDateTimeTrafficImpact<T, string>;
  ReasonCodeText: ReasonCodeText<T>;
  ReasonCode: ReasonCode<T, RawCodeDescription>;
  StartDateTime: string;
  TrafficImpact: TrafficImpact<T, RawTrafficImpact>;
}

export class TrainMessage<T extends TMSchemaVersions> extends Response<T> {
  public affectedLocation: string[];
  public counties: County[];
  public deleted: boolean;
  public endDate: EndDateTime<T>;
  public eventId: string;
  public expectTrafficImpact: ExpectTrafficImpact<T>;
  public externalDescription: string;
  public geometry: Geometry;
  public header: Header<T>;
  public lastUpdate: Date;
  public modified: Date;
  public prognosticatedEndDateTimeTrafficImpact: PrognosticatedEndDateTimeTrafficImpact<T>;
  public reasonCodeText: ReasonCodeText<T>;
  public reasonCode: ReasonCode<T>;
  public startDate: Date;
  public trafficImpact: TrafficImpact<T>;

  constructor(payload: RawTrainMessagePayload<T>, schema: T) {
    super(schema);

    this.affectedLocation = payload.AffectedLocation;
    this.counties = payload.CountyNo;
    this.deleted = payload.Deleted || false;
    this.eventId = payload.EventId;
    this.externalDescription = payload.ExternalDescription;
    this.geometry = parseGeometry(payload.Geometry);
    this.lastUpdate = TrainMessage.parseDate(payload.LastUpdateDateTime);
    this.modified = TrainMessage.parseDate(payload.ModifiedTime);
    this.startDate = TrainMessage.parseDate(payload.StartDateTime);

    if (isActiveSchema(aboveV1Dot3, schema)) {
      this.endDate = TrainMessage.parseDate(payload.EndDateTime) as EndDateTime<T>;
      this.header = payload.Header;
      this.trafficImpact = ((payload.TrafficImpact || []) as RawTrafficImpact[]).map(parseTrafficImpact) as TrafficImpact<T>;
    }

    if (isActiveSchema(aboveV1Dot4, schema)) {
      this.expectTrafficImpact = payload.ExpectTrafficImpact as ExpectTrafficImpact<T>;
      this.prognosticatedEndDateTimeTrafficImpact = TrainMessage.parseDate(
        payload.PrognosticatedEndDateTimeTrafficImpact,
      ) as PrognosticatedEndDateTimeTrafficImpact<T>;
    }

    if (isActiveSchema(aboveV1Dot5, schema)) {
      this.reasonCode = ((payload.ReasonCode || []) as RawCodeDescription[]).map(parseCodeDescription) as ReasonCode<T>;
    } else {
      this.reasonCodeText = payload.ReasonCodeText;
    }
  }
}
