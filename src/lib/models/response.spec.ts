
import 'jest';
import 'jest-extended';
import * as responseTS from './response';

type TestInstance = any;

describe('point.ts', () => {
  describe('exports', () => {
    test('isErrorResponse', () => {
      expect(responseTS.isErrorResponse).toBeDefined();
    });

    test('getFirstErrorResponse', () => {
      expect(responseTS.getFirstErrorResponse).toBeDefined();
    });

    test('hasErrorResponse', () => {
      expect(responseTS.hasErrorResponse).toBeDefined();
    });

    test('Response', () => {
      expect(responseTS.Response).toBeDefined();
    });
  });

  describe('isErrorResponse', () => {
    test('should return true if input is of error type', () => {
      const result: TestInstance = responseTS.isErrorResponse({ ERROR: {} } as TestInstance);

      expect(result).toEqual(true);
    });

    test('should return false if input is not of error type', () => {
      const result: TestInstance = responseTS.isErrorResponse({} as TestInstance);

      expect(result).toEqual(false);
    });
  });

  describe('getFirstErrorResponse', () => {
    test('should return first error found', () => {
      const result: TestInstance = responseTS.getFirstErrorResponse({
        RESPONSE: {
          RESULT: [
            { INFO: { SSEURL: 'test' }},
            { ERROR: { MESSAGE: 'test', SOURCE: 'test' }},
            { ERROR: { MESSAGE: 'test2', SOURCE: 'test2' }}
          ]
        }
      });

      expect(result).toEqual({ ERROR: { MESSAGE: 'test', SOURCE: 'test' }});
    });

    test('should return null if no errors', () => {
      const result: TestInstance = responseTS.getFirstErrorResponse({
        RESPONSE: {
          RESULT: [
            { INFO: { SSEURL: 'test' }}
          ]
        }
      });

      expect(result).toEqual(null);
    });
  });

  describe('hasErrorResponse', () => {
    test('should return true if error found', () => {
      const result: TestInstance = responseTS.hasErrorResponse({
        RESPONSE: {
          RESULT: [
            { INFO: { SSEURL: 'test' }},
            { ERROR: { MESSAGE: 'test', SOURCE: 'test' }},
            { ERROR: { MESSAGE: 'test2', SOURCE: 'test2' }}
          ]
        }
      });

      expect(result).toEqual(true);
    });

    test('should return false if no errors', () => {
      const result: TestInstance = responseTS.hasErrorResponse({
        RESPONSE: {
          RESULT: [
            { INFO: { SSEURL: 'test' }}
          ]
        }
      });

      expect(result).toEqual(false);
    });
  });

  describe('Response', () => {
    describe('constructor', () => {
      test('should set schema on creation', () => {
        const instance: TestInstance = new responseTS.Response('test');

        expect(instance.schema).toEqual('test');
      });
    });

    describe('parseDate', () => {
      test('should be a static function', () => {
        expect((responseTS.Response as TestInstance).parseDate).toBeInstanceOf(Function);
      });

      test('should return a date object when string is present', () => {
        const result: TestInstance = (responseTS.Response as TestInstance).parseDate('12');

        expect(result).toBeInstanceOf(Date);
      });

      test('should return null when invalid value is passed', () => {
        const result: TestInstance = (responseTS.Response as TestInstance).parseDate();

        expect(result).toEqual(null);
      });

      test('should return a date object when string is present', () => {
        const testDate: Date = new Date();
        const result: TestInstance = (responseTS.Response as TestInstance).parseDate(testDate);

        expect(result).toEqual(testDate);
      });
    });
  });
});
