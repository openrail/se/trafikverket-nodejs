import { Response } from './response';

import { County, Country } from '../helpers/county';
import { RawGeometry, Geometry, parseGeometry } from '../helpers/geometry';

export enum TSSchemaVersion {
  'v1' = '1',
}

export type TSSchemaVersions = TSSchemaVersion | '1';
export type DefaultSchemaVersion = '1';
export const defaultSchemaVersion: TSSchemaVersions & DefaultSchemaVersion = '1';

export interface RawTrainStationPayload<T> {
  Advertised: boolean;
  AdvertisedLocationName: string;
  AdvertisedShortLocationName: string;
  CountryCode: string;
  CountyNo: number[];
  Deleted: boolean;
  Geometry: RawGeometry;
  LocationInformationText: string;
  LocationSignature: string;
  ModifiedTime: string;
  PlatformLine: string[];
  Prognosticated: boolean;
}

export class TrainStation<T extends TSSchemaVersions> extends Response<T> {
  public advertised: boolean;
  public advertisedLocationName: string;
  public advertisedShortLocationName: string;
  public countryCode: Country;
  public countyNo: County[];
  public deleted: boolean;
  public geometry: Geometry;
  public locationInformationText: string;
  public locationSignature: string;
  public modified: Date;
  public platformLine: string[];
  public prognosticated: boolean;

  constructor(payload: RawTrainStationPayload<T>, schema: T) {
    super(schema);

    this.advertised = payload.Advertised || false;
    this.advertisedLocationName = payload.AdvertisedLocationName;
    this.advertisedShortLocationName = payload.AdvertisedShortLocationName;
    this.countryCode = payload.CountryCode as Country;
    this.countyNo = payload.CountyNo as County[];
    this.deleted = payload.Deleted || false;
    this.geometry = parseGeometry(payload.Geometry);
    this.locationInformationText = payload.LocationInformationText;
    this.locationSignature = payload.LocationSignature;
    this.modified = TrainStation.parseDate(payload.ModifiedTime);
    this.platformLine = payload.PlatformLine;
    this.prognosticated = payload.Prognosticated || false;
  }
}
