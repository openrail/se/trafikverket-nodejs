
import 'jest';
import 'jest-extended';
import * as pointTS from './point';

type TestInstance = any;

describe('point.ts', () => {
  describe('exports', () => {
    test('PointType', () => {
      expect(pointTS.PointType).toBeDefined();
    });

    test('Point', () => {
      expect(pointTS.Point).toBeDefined();
    });
  });

  describe('PointType', () => {
    test('DataField should be an enum', () => {
      expect(pointTS.PointType).toBeInstanceOf(Object);
    });

    test('All values should be strings', () => {
      const keys: string[] = Object.keys(pointTS.PointType);

      keys.forEach((key: string): void => {
        expect(typeof (pointTS.PointType as { [K: string]: string })[key]).toEqual('string');
        expect((pointTS.PointType as { [K: string]: string })[key]).toEqual(key);
      });
    });
  });

  describe('Point', () => {
    describe('constructor', () => {
      test('should throw an error if not passing in data in correct format for SWEREF99TM', () => {
        expect(() => {
          new pointTS.Point('SWEREF99TM', '(0 0)');
        }).toThrowError();
      });

      test('should throw an error if not passing in data in correct format for WGS84', () => {
        expect(() => {
          new pointTS.Point('WGS84', '(0 0)');
        }).toThrowError();
      });
    });

    describe('SWEREF99TM', () => {
      test('should be a static function', () => {
        expect(pointTS.Point.SWEREF99TM).toBeInstanceOf(Function);
      });

      test('should create an SWEREF99TM instance of Point', () => {
        const instance: TestInstance = pointTS.Point.SWEREF99TM('POINT (0 0)');

        expect(instance).toBeInstanceOf(pointTS.Point);
        expect(instance.type).toEqual('SWEREF99TM');
      });

      test('should parse data from "POINT (n n)" format', () => {
        const instance: TestInstance = pointTS.Point.SWEREF99TM('POINT (12.5 7.7)');

        expect(instance.easting).toEqual(12.5);
        expect(instance.northing).toEqual(7.7);
      });

      test('should parse data from easting, northing format', () => {
        const instance: TestInstance = pointTS.Point.SWEREF99TM(12.5, 7.7);

        expect(instance.easting).toEqual(12.5);
        expect(instance.northing).toEqual(7.7);
      });

      test('should stringify correctly', () => {
        const instance: TestInstance = pointTS.Point.SWEREF99TM(12.5, 7.7);

        expect(instance.toString()).toEqual('POINT (12.5 7.7)');
      });

      test('should stringify correctly with type', () => {
        const instance: TestInstance = pointTS.Point.SWEREF99TM(12.5, 7.7);

        expect(instance.toString(true)).toEqual('SWEREF99TM (12.5 7.7)');
      });

      test('should convert into json object without type', () => {
        const instance: TestInstance = pointTS.Point.SWEREF99TM(12.5, 7.7);

        expect(instance.toJSON()).toEqual({ easting: 12.5, northing: 7.7 });
      });

      test('should convert into json object with type', () => {
        const instance: TestInstance = pointTS.Point.SWEREF99TM(12.5, 7.7);

        expect(instance.toJSON(true)).toEqual({ type: 'SWEREF99TM', easting: 12.5, northing: 7.7 });
      });
    });

    describe('WGS84', () => {
      test('should be a static function', () => {
        expect(pointTS.Point.WGS84).toBeInstanceOf(Function);
      });

      test('should create an WGS84 instance of Point', () => {
        const instance: TestInstance = pointTS.Point.WGS84('POINT (0 0)');

        expect(instance).toBeInstanceOf(pointTS.Point);
        expect(instance.type).toEqual('WGS84');
      });

      test('should parse data from "POINT (n n)" format', () => {
        const instance: TestInstance = pointTS.Point.WGS84('POINT (12.5 7.7)');

        expect(instance.latitude).toEqual(12.5);
        expect(instance.longitude).toEqual(7.7);
      });

      test('should parse data from latitude, longitude format', () => {
        const instance: TestInstance = pointTS.Point.WGS84(12.5, 7.7);

        expect(instance.latitude).toEqual(12.5);
        expect(instance.longitude).toEqual(7.7);
      });

      test('should stringify correctly', () => {
        const instance: TestInstance = pointTS.Point.WGS84(12.5, 7.7);

        expect(instance.toString()).toEqual('POINT (12.5 7.7)');
      });

      test('should stringify correctly with type', () => {
        const instance: TestInstance = pointTS.Point.WGS84(12.5, 7.7);

        expect(instance.toString(true)).toEqual('WGS84 (12.5 7.7)');
      });

      test('should convert into json object without type', () => {
        const instance: TestInstance = pointTS.Point.WGS84(12.5, 7.7);

        expect(instance.toJSON()).toEqual({ latitude: 12.5, longitude: 7.7 });
      });

      test('should convert into json object with type', () => {
        const instance: TestInstance = pointTS.Point.WGS84(12.5, 7.7);

        expect(instance.toJSON(true)).toEqual({ type: 'WGS84', latitude: 12.5, longitude: 7.7 });
      });
    });
  });
});
