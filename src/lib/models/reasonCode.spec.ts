
import 'jest';
import 'jest-extended';
import * as reasonCodeTS from './reasonCode';

type TestInstance = any;

const mockdata: TestInstance = {
  Code: 'TEST',
  Deleted: false,
  GroupDescription: 'test',
  Level1Description: 'test1',
  Level2Description: 'test2',
  Level3Description: 'test3',
  ModifiedTime: new Date().toISOString(),
};

describe('reasonCode.ts', () => {
  describe('exports', () => {
    test('defaultSchemaVersion', () => {
      expect(reasonCodeTS.defaultSchemaVersion).toBeDefined();
    });

    test('ReasonCode', () => {
      expect(reasonCodeTS.ReasonCode).toBeDefined();
    });
  });

  describe('defaultSchemaVersion', () => {
    test('default schema should be 1', () => {
      expect(reasonCodeTS.defaultSchemaVersion).toEqual('1');
    });
  });

  describe('ReasonCode', () => {
    describe('constructor', () => {
      test('should set schema on creation', () => {
        const instance: TestInstance = new reasonCodeTS.ReasonCode(mockdata, '1');

        expect(instance.schema).toEqual('1');
      });

      test('should set code on creation', () => {
        const instance: TestInstance = new reasonCodeTS.ReasonCode(mockdata, '1');

        expect(instance.code).toBeString();
        expect(instance.code).toEqual(mockdata.Code);
      });

      test('should set deleted on creation', () => {
        const instance: TestInstance = new reasonCodeTS.ReasonCode(mockdata, '1');

        expect(instance.deleted).toBeBoolean();
        expect(instance.deleted).toEqual(mockdata.Deleted);
      });

      test('should set groupDescription on creation', () => {
        const instance: TestInstance = new reasonCodeTS.ReasonCode(mockdata, '1');

        expect(instance.groupDescription).toBeString();
        expect(instance.groupDescription).toEqual(mockdata.GroupDescription);
      });

      test('should set level1Description on creation', () => {
        const instance: TestInstance = new reasonCodeTS.ReasonCode(mockdata, '1');

        expect(instance.level1Description).toBeString();
        expect(instance.level1Description).toEqual(mockdata.Level1Description);
      });

      test('should set level2Description on creation', () => {
        const instance: TestInstance = new reasonCodeTS.ReasonCode(mockdata, '1');

        expect(instance.level2Description).toBeString();
        expect(instance.level2Description).toEqual(mockdata.Level2Description);
      });

      test('should set level3Description on creation', () => {
        const instance: TestInstance = new reasonCodeTS.ReasonCode(mockdata, '1');

        expect(instance.level3Description).toBeString();
        expect(instance.level3Description).toEqual(mockdata.Level3Description);
      });

      test('should set modified on creation', () => {
        const instance: TestInstance = new reasonCodeTS.ReasonCode(mockdata, '1');

        expect(instance.modified).toBeInstanceOf(Date);
        expect(instance.modified).toEqual(new Date(mockdata.ModifiedTime));
      });
    });
  });
});
