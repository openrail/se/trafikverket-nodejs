import { EventEmitter } from 'events';
import { ClientRequest, IncomingMessage } from 'http';
import { request, RequestOptions } from 'https';
import { URL } from 'url';

import { RawResponse, getFirstErrorResponse, RawResult } from '../models/response';

enum EventSourceStates {
  uninitialised = 'uninitialised',
  pending = 'pending',
  active = 'active',
  idle = 'idle',
  dead = 'dead',
};

// TODO: get events coupled onto T generic to return correct data types in typings

export class EventSourceListner<T> extends EventEmitter {
  private url: URL;
  private request: ClientRequest;
  private state: EventSourceStates = EventSourceStates.uninitialised;
  private reconnect: boolean;
  private lastId: string;
  private heartbeatTimout: NodeJS.Timeout;
  private idleTimeout: NodeJS.Timeout;

  constructor(url: string | URL, reconnect: boolean = false) {
    super();

    this.url = (url instanceof URL) ? url : new URL(url);
    this.reconnect = reconnect;
  }

  private heartbeat(): void {
    if (this.heartbeatTimout) {
      this.heartbeatTimout.refresh();
    } else {
      this.heartbeatTimout = setTimeout(((): void => {
        this.state = EventSourceStates.dead;
        this.close(true);
        // TODO: handle failed heatbeat here....
      }).bind(this), 65000); // should fail 2 heartbeats at 30 seconds each with 5 second buffer
    }

    if (this.idleTimeout) {
      this.idleTimeout.refresh();
    } else {
      this.heartbeatTimout = setTimeout(((): void => {
        this.state = EventSourceStates.idle;
      }).bind(this), 32500); // set idle if we miss 1 heartbeat with 2.5 second buffer
    }

    this.state = EventSourceStates.active;
  }


  private handleResponse(res: IncomingMessage): void {
    res.on('data', ((chunk: Buffer): void => {
      this.heartbeat();

      const response: string = chunk.toString().trim();

      if (response === ':Heartbeat') {
        this.emit('heartbeat');
      } else if (response === ':Connected') {
        this.emit('connected');
      } else if (response.startsWith('id: ')) {
        const responseLines: string[] = response.split('\n');
        const id: string = responseLines[0].replace('id: ', '');
        const data: RawResponse = JSON.parse(responseLines[1].replace('data: ', ''));

        this.lastId = id.trim();

        const results: RawResult[] = data.RESPONSE.RESULT || [];

        results.forEach((result: RawResult) => {
          this.emit('message', result);
        });

      } else if (response.startsWith('{') && response.endsWith('}')) {
        const jsonResponse: RawResponse = JSON.parse(response);

        const apiError: RawResult = getFirstErrorResponse(jsonResponse);

        if (apiError) {
          this.emit('error', new Error(`${apiError.ERROR.MESSAGE}\n${apiError.ERROR.SOURCE}`));
          this.close();
        } else {
          console.warn(`Unhandled event type: "${response}"`);
        }
      } else {
        console.warn(`Unhandled event type: "${response}"`);
      }
    }).bind(this));

    res.on('end', () => {
      this.close();
    });

    res.on('error', (err) => {
      this.emit('error', err);
      this.close();
    });
    res.on('close', () => {
      this.close();
    });
  }

  public connect(): void {
    if (!this.request) {
      this.state = EventSourceStates.pending;

      if (this.lastId) {
        this.url.searchParams.append('lasteventid', this.lastId);
      }

      const requestOptions: RequestOptions = {
        agent: false,
        protocol: this.url.protocol,
        host: this.url.hostname,
        port: this.url.port,
        path: `${this.url.pathname}${this.url.search}`,
        method: 'GET',
        headers: {
          accept: 'text/event-stream'
        }
      };

      this.request = request(requestOptions, this.handleResponse.bind(this));

      this.request.on('error', (err: Error): void => {
        this.emit('error', err);
        this.close();
      });
      this.request.on('abort', (err: Error): void => {
        this.emit('abort', err);
        this.close();
      });

      this.request.end();
    } else {
      throw new Error('Listner has already been called to connect');
    }
  }

  public close(force: boolean = false): void {
    this.request = null;

    if (this.reconnect === true && force === false) {
      this.state = EventSourceStates.uninitialised;
      this.emit('reconnecting');
      this.connect();
    } else {
      this.state = EventSourceStates.dead;
      this.emit('closed');
    }
  }

  public on(event: 'heartbeat', listener: () => void): this;
  public on(event: 'connected', listener: () => void): this;
  public on(event: 'message', listener: (data: T & RawResult) => void): this;
  public on(event: 'error', listener: (err: Error) => void): this;
  public on(event: 'reconnecting', listener: () => void): this;
  public on(event: 'closed', listener: () => void): this;
  public on(event: string, listener: (...args: any[]) => any): this {
    super.on(event, listener);

    return this;
  }
}
