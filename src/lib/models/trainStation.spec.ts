
import 'jest';
import 'jest-extended';
import * as trainStationTS from './trainStation';

import { Point } from './point';

type TestInstance = any;

const mockdata: TestInstance = {
  Advertised: false,
  AdvertisedLocationName: 'test',
  AdvertisedShortLocationName: 'test',
  CountryCode: 'SE',
  CountyNo: [1],
  Deleted: false,
  Geometry: {
    SWEREF99TM: 'POINT (0 0)',
    WGS84: 'POINT (0 0)'
  },
  LocationInformationText: 'test',
  LocationSignature: 'test',
  ModifiedTime: new Date().toISOString(),
  PlatformLine: '1',
  Prognosticated: false,
};

describe('trainStation.ts', () => {
  describe('exports', () => {
    test('defaultSchemaVersion', () => {
      expect(trainStationTS.defaultSchemaVersion).toBeDefined();
    });

    test('TrainStation', () => {
      expect(trainStationTS.TrainStation).toBeDefined();
    });
  });

  describe('defaultSchemaVersion', () => {
    test('default schema should be 1', () => {
      expect(trainStationTS.defaultSchemaVersion).toEqual('1');
    });
  });

  describe('TrainStation', () => {
    describe('constructor', () => {
      test('should set schema on creation', () => {
        const instance: TestInstance = new trainStationTS.TrainStation(mockdata, '1');

        expect(instance.schema).toEqual('1');
      });

      test('should set advertised on creation', () => {
        const instance: TestInstance = new trainStationTS.TrainStation(mockdata, '1');

        expect(instance.advertised).toBeBoolean();
        expect(instance.advertised).toEqual(mockdata.Advertised);
      });

      test('should set advertisedLocationName on creation', () => {
        const instance: TestInstance = new trainStationTS.TrainStation(mockdata, '1');

        expect(instance.advertisedLocationName).toBeString();
        expect(instance.advertisedLocationName).toEqual(mockdata.AdvertisedLocationName);
      });

      test('should set advertisedShortLocationName on creation', () => {
        const instance: TestInstance = new trainStationTS.TrainStation(mockdata, '1');

        expect(instance.advertisedShortLocationName).toBeString();
        expect(instance.advertisedShortLocationName).toEqual(mockdata.AdvertisedShortLocationName);
      });

      test('should set countryCode on creation', () => {
        const instance: TestInstance = new trainStationTS.TrainStation(mockdata, '1');

        expect(instance.countryCode).toBeString();
        expect(instance.countryCode).toEqual(mockdata.CountryCode);
      });

      test('should set countyNo on creation', () => {
        const instance: TestInstance = new trainStationTS.TrainStation(mockdata, '1');

        expect(instance.countyNo).toBeArray();
        expect(instance.countyNo).toEqual(mockdata.CountyNo);
      });

      test('should set deleted on creation', () => {
        const instance: TestInstance = new trainStationTS.TrainStation(mockdata, '1');

        expect(instance.deleted).toBeBoolean();
        expect(instance.deleted).toEqual(mockdata.Deleted);
      });

      test('should set geometry on creation', () => {
        const instance: TestInstance = new trainStationTS.TrainStation(mockdata, '1');

        expect(instance.geometry).toBeObject();
        expect(instance.geometry).toContainAllKeys(['SWEREF99TM', 'WGS84']);
      });

      test('should set geometry.SWEREF99TM on creation', () => {
        const instance: TestInstance = new trainStationTS.TrainStation(mockdata, '1');

        expect(instance.geometry.SWEREF99TM).toBeInstanceOf(Point);
        expect(instance.geometry.SWEREF99TM).toEqual(Point.SWEREF99TM(mockdata.Geometry.SWEREF99TM));
      });

      test('should set geometry.WGS84 on creation', () => {
        const instance: TestInstance = new trainStationTS.TrainStation(mockdata, '1');

        expect(instance.geometry.WGS84).toBeInstanceOf(Point);
        expect(instance.geometry.WGS84).toEqual(Point.WGS84(mockdata.Geometry.WGS84));
      });

      test('should set locationInformationText on creation', () => {
        const instance: TestInstance = new trainStationTS.TrainStation(mockdata, '1');

        expect(instance.locationInformationText).toBeString();
        expect(instance.locationInformationText).toEqual(mockdata.LocationInformationText);
      });

      test('should set locationSignature on creation', () => {
        const instance: TestInstance = new trainStationTS.TrainStation(mockdata, '1');

        expect(instance.locationSignature).toBeString();
        expect(instance.locationSignature).toEqual(mockdata.LocationSignature);
      });

      test('should set modified on creation', () => {
        const instance: TestInstance = new trainStationTS.TrainStation(mockdata, '1');

        expect(instance.modified).toBeInstanceOf(Date);
        expect(instance.modified).toEqual(new Date(mockdata.ModifiedTime));
      });

      test('should set platformLine on creation', () => {
        const instance: TestInstance = new trainStationTS.TrainStation(mockdata, '1');

        expect(instance.platformLine).toBeString();
        expect(instance.platformLine).toEqual(mockdata.PlatformLine);
      });

      test('should set prognosticated on creation', () => {
        const instance: TestInstance = new trainStationTS.TrainStation(mockdata, '1');

        expect(instance.prognosticated).toBeBoolean();
        expect(instance.prognosticated).toEqual(mockdata.Prognosticated);
      });
    });
  });
});
