
import 'jest';
import 'jest-extended';
import * as trainAnnouncementTS from './trainAnnouncement';

type TestInstance = any;

const mockdataV1: TestInstance = {
  ActivityId: 'test',
  ActivityType: 'test',
  Advertised: false,
  AdvertisedTimeAtLocation: new Date().toISOString(),
  AdvertisedTrainIdent: 'test',
  Booking: ['test'],
  Cancled: false,
  Deleted: false,
  Deviation: ['test'],
  EstimatedTimeAtLocation: new Date().toISOString(),
  EstimatedTimeIsPreliminary: true,
  FromLocation: ['test'],
  LocationSignature: 'test',
  MobileWebLink: 'test',
  ModifiedTime: new Date().toISOString(),
  OtherInformation: ['test'],
  ProductInformation: ['test'],
  ScheduledDepartureDateTime: new Date().toISOString(),
  Service: ['test'],
  TimeAtLocation: new Date().toISOString(),
  TrackAtLocation: 'test',
  TrainComposition: ['test'],
  ToLocation: ['test'],
  TypeOfTraffic: 'test',
  WebLink: 'test'
};

const mockdataV1Dot1: TestInstance = Object.assign({}, mockdataV1, {
  FromLocation: [{ LocationName: 'test', Order: 1, Priority: 2 }],
  NewEquipment: 3,
  PlannedEstimatedTimeAtLocation: new Date().toISOString(),
  PlannedEstimatedTimeAtLocationIsValid: true,
  TechnicalTrainIdent: 'test',
  ToLocation: [{ LocationName: 'test', Order: 1, Priority: 2 }],
  ViaFromLocation: [{ LocationName: 'test', Order: 1, Priority: 2 }],
  ViaToLocation: [{ LocationName: 'test', Order: 1, Priority: 2 }]
});

const mockdataV1Dot3: TestInstance = Object.assign({}, mockdataV1Dot1, {
  WebLinkName: 'test'
});

const mockdataV1Dot4: TestInstance = Object.assign({}, mockdataV1Dot3, {
  Operator: 'test',
  TechnicalDateTime: new Date().toISOString(),
  TimeAtLocationWithSeconds: new Date().toISOString(),
  TrainOwner: 'test'
});

const mockdataV1Dot5: TestInstance = Object.assign({}, mockdataV1Dot4, {
  Booking: [{ Code: 'test', Description: 'test' }],
  Deviation: [{ Code: 'test', Description: 'test' }],
  OtherInformation: [{ Code: 'test', Description: 'test' }],
  ProductInformation: [{ Code: 'test', Description: 'test' }],
  Service: [{ Code: 'test', Description: 'test' }],
  TrainComposition: [{ Code: 'test', Description: 'test' }]
});

const mockdataV1Dot6: TestInstance = Object.assign({}, mockdataV1Dot5, {
  TypeOfTraffic: [{ Code: 'test', Description: 'test' }]
});

function instanceDatacheckV1(
  mockdata: TestInstance,
  schema: trainAnnouncementTS.TASchemaVersions
): void {
  test('should set schema on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    expect(instance.schema).toEqual(schema);
  });

  test('should set activityId on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    expect(instance.activityId).toBeString();
    expect(instance.activityId).toEqual(mockdata.ActivityId);
  });

  test('should set advertised on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    expect(instance.advertised).toBeBoolean();
    expect(instance.advertised).toEqual(mockdata.Advertised);
  });

  test('should set advertisedTimeAtLocation on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    expect(instance.advertisedTimeAtLocation).toBeInstanceOf(Date);
    expect(instance.advertisedTimeAtLocation).toEqual(new Date(mockdata.AdvertisedTimeAtLocation));
  });

  test('should set advertisedTrainIdentity on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    expect(instance.advertisedTrainIdentity).toBeString();
    expect(instance.advertisedTrainIdentity).toEqual(mockdata.AdvertisedTrainIdent);
  });

  test('should set cancled on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    expect(instance.cancled).toBeBoolean();
    expect(instance.cancled).toEqual(mockdata.Cancled);
  });

  test('should set deleted on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    expect(instance.deleted).toBeBoolean();
    expect(instance.deleted).toEqual(mockdata.Deleted);
  });

  test('should set estimtedTimeAtLocation on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    expect(instance.estimtedTimeAtLocation).toBeInstanceOf(Date);
    expect(instance.estimtedTimeAtLocation).toEqual(new Date(mockdata.EstimatedTimeAtLocation));
  });

  test('should set estimatedTimeIsPreliminary on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    expect(instance.estimatedTimeIsPreliminary).toBeBoolean();
    expect(instance.estimatedTimeIsPreliminary).toEqual(mockdata.EstimatedTimeIsPreliminary);
  });

  test('should set locationSignature on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    expect(instance.locationSignature).toBeString();
    expect(instance.locationSignature).toEqual(mockdata.LocationSignature);
  });

  test('should set mobileWebLink on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    expect(instance.mobileWebLink).toBeString();
    expect(instance.mobileWebLink).toEqual(mockdata.MobileWebLink);
  });

  test('should set modifiedTime on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    expect(instance.modifiedTime).toBeInstanceOf(Date);
    expect(instance.modifiedTime).toEqual(new Date(mockdata.ModifiedTime));
  });

  test('should set scheduledDepartureDateTime on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    expect(instance.scheduledDepartureDateTime).toBeInstanceOf(Date);
    expect(instance.scheduledDepartureDateTime).toEqual(new Date(mockdata.ScheduledDepartureDateTime));
  });

  test('should set timeAtLocation on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    expect(instance.timeAtLocation).toBeInstanceOf(Date);
    expect(instance.timeAtLocation).toEqual(new Date(mockdata.TimeAtLocation));
  });

  test('should set trackAtLocation on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    expect(instance.trackAtLocation).toBeString();
    expect(instance.trackAtLocation).toEqual(mockdata.TrackAtLocation);
  });

  test('should set webLink on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    expect(instance.webLink).toBeString();
    expect(instance.webLink).toEqual(mockdata.WebLink);
  });
}

function instanceDatacheckV1Dot1(
  mockdata: TestInstance,
  schema: trainAnnouncementTS.TASchemaVersions,
  aboveSchema: boolean,
): void {
  test('should set fromLocation on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    if (aboveSchema) {
      expect(instance.fromLocation).toBeArray();
      expect(instance.fromLocation).toEqual([{ name: 'test', order: 1, priority: 2 }]);
    } else {
      expect(instance.fromLocation).toBeArray();
      expect(instance.fromLocation).toEqual(['test']);
    }
  });

  test('should set newEquipment on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    if (aboveSchema) {
      expect(instance.newEquipment).toBeNumber();
      expect(instance.newEquipment).toEqual(mockdata.NewEquipment);
    } else {
      expect(instance.newEquipment).toBeUndefined();
    }
  });

  test('should set plannedEstimatedTimeAtLocation on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    if (aboveSchema) {
      expect(instance.plannedEstimatedTimeAtLocation).toBeInstanceOf(Date);
      expect(instance.plannedEstimatedTimeAtLocation).toEqual(new Date(mockdata.PlannedEstimatedTimeAtLocation));
    } else {
      expect(instance.plannedEstimatedTimeAtLocation).toBeUndefined();
    }
  });

  test('should set plannedEstimatedTimeAtLocationIsValid on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    if (aboveSchema) {
      expect(instance.plannedEstimatedTimeAtLocationIsValid).toBeBoolean();
      expect(instance.plannedEstimatedTimeAtLocationIsValid).toEqual(mockdata.PlannedEstimatedTimeAtLocationIsValid);
    } else {
      expect(instance.plannedEstimatedTimeAtLocationIsValid).toBeUndefined();
    }
  });

  test('should set technicalTrainIdentity on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    if (aboveSchema) {
      expect(instance.technicalTrainIdentity).toBeString();
      expect(instance.technicalTrainIdentity).toEqual(mockdata.TechnicalTrainIdent);
    } else {
      expect(instance.technicalTrainIdentity).toBeUndefined();
    }
  });

  test('should set toLocation on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    if (aboveSchema) {
      expect(instance.toLocation).toBeArray();
      expect(instance.toLocation).toEqual([{ name: 'test', order: 1, priority: 2 }]);
    } else {
      expect(instance.toLocation).toBeArray();
      expect(instance.toLocation).toEqual(['test']);
    }
  });

  test('should set viaFromLocation on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    if (aboveSchema) {
      expect(instance.viaFromLocation).toBeArray();
      expect(instance.viaFromLocation).toEqual([{ name: 'test', order: 1, priority: 2 }]);
    } else {
      expect(instance.viaFromLocation).toBeUndefined();
    }
  });

  test('should set viaToLocation on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    if (aboveSchema) {
      expect(instance.viaToLocation).toBeArray();
      expect(instance.viaToLocation).toEqual([{ name: 'test', order: 1, priority: 2 }]);
    } else {
      expect(instance.viaToLocation).toBeUndefined();
    }
  });
}

function instanceDatacheckV1Dot3(
  mockdata: TestInstance,
  schema: trainAnnouncementTS.TASchemaVersions,
  aboveSchema: boolean,
): void {
  test('should set webLinkName on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    if (aboveSchema) {
      expect(instance.webLinkName).toBeString();
      expect(instance.webLinkName).toEqual(mockdata.WebLinkName);
    } else {
      expect(instance.webLinkName).toBeUndefined();
    }
  });
}

function instanceDatacheckV1Dot4(
  mockdata: TestInstance,
  schema: trainAnnouncementTS.TASchemaVersions,
  aboveSchema: boolean,
): void {
  test('should set operator on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    if (aboveSchema) {
      expect(instance.operator).toBeString();
      expect(instance.operator).toEqual(mockdata.Operator);
    } else {
      expect(instance.operator).toBeUndefined();
    }
  });

  test('should set technicalDateTime on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    if (aboveSchema) {
      expect(instance.technicalDateTime).toBeInstanceOf(Date);
      expect(instance.technicalDateTime).toEqual(new Date(mockdata.TechnicalDateTime));
    } else {
      expect(instance.technicalDateTime).toBeUndefined();
    }
  });

  test('should set timeAtLocationWithSeconds on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    if (aboveSchema) {
      expect(instance.timeAtLocationWithSeconds).toBeInstanceOf(Date);
      expect(instance.timeAtLocationWithSeconds).toEqual(new Date(mockdata.TimeAtLocationWithSeconds));
    } else {
      expect(instance.timeAtLocationWithSeconds).toBeUndefined();
    }
  });

  test('should set trainOwner on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    if (aboveSchema) {
      expect(instance.trainOwner).toBeString();
      expect(instance.trainOwner).toEqual(mockdata.TrainOwner);
    } else {
      expect(instance.trainOwner).toBeUndefined();
    }
  });
}

function instanceDatacheckV1Dot5(
  mockdata: TestInstance,
  schema: trainAnnouncementTS.TASchemaVersions,
  aboveSchema: boolean,
): void {
  test('should set booking on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    if (aboveSchema) {
      expect(instance.booking).toBeArray();
      expect(instance.booking).toEqual([{ code: 'test', description: 'test' }]);
    } else {
      expect(instance.booking).toBeArray();
      expect(instance.booking).toEqual(['test']);
    }
  });

  test('should set deviation on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    if (aboveSchema) {
      expect(instance.deviation).toBeArray();
      expect(instance.deviation).toEqual([{ code: 'test', description: 'test' }]);
    } else {
      expect(instance.deviation).toBeArray();
      expect(instance.deviation).toEqual(['test']);
    }
  });

  test('should set otherInformation on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    if (aboveSchema) {
      expect(instance.otherInformation).toBeArray();
      expect(instance.otherInformation).toEqual([{ code: 'test', description: 'test' }]);
    } else {
      expect(instance.otherInformation).toBeArray();
      expect(instance.otherInformation).toEqual(['test']);
    }
  });

  test('should set productInformation on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    if (aboveSchema) {
      expect(instance.productInformation).toBeArray();
      expect(instance.productInformation).toEqual([{ code: 'test', description: 'test' }]);
    } else {
      expect(instance.productInformation).toBeArray();
      expect(instance.productInformation).toEqual(['test']);
    }
  });

  test('should set service on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    if (aboveSchema) {
      expect(instance.service).toBeArray();
      expect(instance.service).toEqual([{ code: 'test', description: 'test' }]);
    } else {
      expect(instance.service).toBeArray();
      expect(instance.service).toEqual(['test']);
    }
  });

  test('should set trainComposition on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    if (aboveSchema) {
      expect(instance.trainComposition).toBeArray();
      expect(instance.trainComposition).toEqual([{ code: 'test', description: 'test' }]);
    } else {
      expect(instance.trainComposition).toBeArray();
      expect(instance.trainComposition).toEqual(['test']);
    }
  });
}

function instanceDatacheckV1Dot6(
  mockdata: TestInstance,
  schema: trainAnnouncementTS.TASchemaVersions,
  aboveSchema: boolean,
): void {
  test('should set typeOfTraffic on creation', () => {
    const instance: TestInstance = new trainAnnouncementTS.TrainAnnouncement(mockdata, schema);

    if (aboveSchema) {
      expect(instance.typeOfTraffic).toBeArray();
      expect(instance.typeOfTraffic).toEqual([{ code: 'test', description: 'test' }]);
    } else {
      expect(instance.typeOfTraffic).toBeString();
      expect(instance.typeOfTraffic).toEqual(mockdata.TypeOfTraffic);
    }
  });
}


describe('trainMessage.ts', () => {
  describe('exports', () => {
    test('defaultSchemaVersion', () => {
      expect(trainAnnouncementTS.defaultSchemaVersion).toBeDefined();
    });

    test('TrainAnnouncement', () => {
      expect(trainAnnouncementTS.TrainAnnouncement).toBeDefined();
    });
  });

  describe('defaultSchemaVersion', () => {
    test('default schema should be 1.6', () => {
      expect(trainAnnouncementTS.defaultSchemaVersion).toEqual('1.6');
    });
  });
  describe('constructor', () => {
    describe('schema 1', () => {
      const schema: trainAnnouncementTS.TASchemaVersions = '1';
      const mockdata: TestInstance = mockdataV1;

      instanceDatacheckV1(mockdata, schema);
      instanceDatacheckV1Dot1(mockdata, schema, false);
      instanceDatacheckV1Dot3(mockdata, schema, false);
      instanceDatacheckV1Dot4(mockdata, schema, false);
      instanceDatacheckV1Dot5(mockdata, schema, false);
      instanceDatacheckV1Dot6(mockdata, schema, false);
    });

    describe('schema 1.1', () => {
      const schema: trainAnnouncementTS.TASchemaVersions = '1.1';
      const mockdata: TestInstance = mockdataV1Dot1;

      instanceDatacheckV1(mockdata, schema);
      instanceDatacheckV1Dot1(mockdata, schema, true);
      instanceDatacheckV1Dot3(mockdata, schema, false);
      instanceDatacheckV1Dot4(mockdata, schema, false);
      instanceDatacheckV1Dot5(mockdata, schema, false);
      instanceDatacheckV1Dot6(mockdata, schema, false);
    });

    describe('schema 1.3', () => {
      const schema: trainAnnouncementTS.TASchemaVersions = '1.3';
      const mockdata: TestInstance = mockdataV1Dot3;

      instanceDatacheckV1(mockdata, schema);
      instanceDatacheckV1Dot1(mockdata, schema, true);
      instanceDatacheckV1Dot3(mockdata, schema, true);
      instanceDatacheckV1Dot4(mockdata, schema, false);
      instanceDatacheckV1Dot5(mockdata, schema, false);
      instanceDatacheckV1Dot6(mockdata, schema, false);
    });

    describe('schema 1.4', () => {
      const schema: trainAnnouncementTS.TASchemaVersions = '1.4';
      const mockdata: TestInstance = mockdataV1Dot4;

      instanceDatacheckV1(mockdata, schema);
      instanceDatacheckV1Dot1(mockdata, schema, true);
      instanceDatacheckV1Dot3(mockdata, schema, true);
      instanceDatacheckV1Dot4(mockdata, schema, true);
      instanceDatacheckV1Dot5(mockdata, schema, false);
      instanceDatacheckV1Dot6(mockdata, schema, false);
    });

    describe('schema 1.5', () => {
      const schema: trainAnnouncementTS.TASchemaVersions = '1.5';
      const mockdata: TestInstance = mockdataV1Dot5;

      instanceDatacheckV1(mockdata, schema);
      instanceDatacheckV1Dot1(mockdata, schema, true);
      instanceDatacheckV1Dot3(mockdata, schema, true);
      instanceDatacheckV1Dot4(mockdata, schema, true);
      instanceDatacheckV1Dot5(mockdata, schema, true);
      instanceDatacheckV1Dot6(mockdata, schema, false);
    });

    describe('schema 1.6', () => {
      const schema: trainAnnouncementTS.TASchemaVersions = '1.6';
      const mockdata: TestInstance = mockdataV1Dot6;

      instanceDatacheckV1(mockdata, schema);
      instanceDatacheckV1Dot1(mockdata, schema, true);
      instanceDatacheckV1Dot3(mockdata, schema, true);
      instanceDatacheckV1Dot4(mockdata, schema, true);
      instanceDatacheckV1Dot5(mockdata, schema, true);
      instanceDatacheckV1Dot6(mockdata, schema, true);
    });
  });
});
