import 'jest';
import 'jest-extended';
import * as eventSourceListnerTS from './eventSourceListner';
import { URL } from 'url';
import { EventEmitter } from 'events';

jest.useFakeTimers();

type TestInstance = any;

class MockIncommingMessage extends EventEmitter {};

describe('models/eventSourceListner.ts', () => {
  describe('exports', () => {
    test('EventSourceListner', () => {
      expect(eventSourceListnerTS.EventSourceListner).toBeDefined();
    });
  });

  describe('TrainStationQuery', () => {
    beforeEach(() => {
      jest.clearAllTimers();
    });

    describe('constructor', () => {
      test('sets the URL as a URL object from string', () => {
        const instance: TestInstance = new eventSourceListnerTS.EventSourceListner('http://example.com');

        expect(instance.url).toBeInstanceOf(URL);
        expect(instance.url.href).toEqual('http://example.com/');
      });

      test('sets the URL as a URL object from URL object', () => {
        const mockURL: URL = new URL('http://example.com');
        const instance: TestInstance = new eventSourceListnerTS.EventSourceListner(mockURL);

        expect(instance.url).toBeInstanceOf(URL);
        expect(instance.url.href).toEqual(mockURL.href);
      });

      test('sets reconnection to true', () => {
        const instance: TestInstance = new eventSourceListnerTS.EventSourceListner('http://example.com', true);

        expect(instance.reconnect).toBeBoolean();
        expect(instance.reconnect).toEqual(true);
      });

      test('sets reconnection to false', () => {
        const instance: TestInstance = new eventSourceListnerTS.EventSourceListner('http://example.com', false);

        expect(instance.reconnect).toBeBoolean();
        expect(instance.reconnect).toEqual(false);
      });

      test('defaults reconnection to false', () => {
        const instance: TestInstance = new eventSourceListnerTS.EventSourceListner('http://example.com');

        expect(instance.reconnect).toBeBoolean();
        expect(instance.reconnect).toEqual(false);
      });
    });

    describe('heartbeat', () => {
      test('should set heartbeat timeout', () => {
        const instance: TestInstance = new eventSourceListnerTS.EventSourceListner('http://example.com');

        expect(instance.heartbeatTimout).toBeUndefined();
        
        instance.heartbeat();

        expect(instance.heartbeatTimout).toBeDefined();
        expect(instance.state).toEqual('active');
      });

      test('should set idle state after 35 seconds', () => {
        const instance: TestInstance = new eventSourceListnerTS.EventSourceListner('http://example.com');

        expect(instance.state).toEqual('uninitialised');

        instance.heartbeat();

        expect(instance.state).toEqual('active');

        jest.advanceTimersByTime(35000);

        expect(instance.state).toEqual('idle');
      });

      test('should set dead state after 65 seconds', () => {
        const instance: TestInstance = new eventSourceListnerTS.EventSourceListner('http://example.com');

        expect(instance.state).toEqual('uninitialised');
        
        instance.heartbeat();

        expect(instance.state).toEqual('active');

        jest.advanceTimersByTime(66000);

        expect(instance.state).toEqual('dead');
      });
    });

    describe('handleResponse', () => {
      test('sets state to dead and emits closed when response ends', (done) => {
        const instance: TestInstance = new eventSourceListnerTS.EventSourceListner('http://example.com');
        const mockIncommingResponse = new MockIncommingMessage();

        expect(instance.state).toEqual('uninitialised');

        instance.handleResponse(mockIncommingResponse);

        instance.on('closed', () => {
          expect(instance.state).toEqual('dead');
          done();
        });

        mockIncommingResponse.emit('end');
      });

      test('sets state to dead and emits closed when response errors', (done) => {
        const instance: TestInstance = new eventSourceListnerTS.EventSourceListner('http://example.com');
        const mockIncommingResponse = new MockIncommingMessage();

        expect(instance.state).toEqual('uninitialised');

        instance.handleResponse(mockIncommingResponse);

        instance.on('error', () => { /* empty */ });
        instance.on('closed', () => {
          expect(instance.state).toEqual('dead');
          done();
        });

        mockIncommingResponse.emit('error', new Error('test error'));
      });

      test('emits error when response errors', (done) => {
        const instance: TestInstance = new eventSourceListnerTS.EventSourceListner('http://example.com');
        const mockIncommingResponse = new MockIncommingMessage();

        expect(instance.state).toEqual('uninitialised');

        instance.handleResponse(mockIncommingResponse);

        instance.on('error', (err: any) => {
          expect(err).toBeInstanceOf(Error);
          done();
        });

        mockIncommingResponse.emit('error', new Error('test error'));
      });

      test('sets state to dead and emits closed when response closes', (done) => {
        const instance: TestInstance = new eventSourceListnerTS.EventSourceListner('http://example.com');
        const mockIncommingResponse = new MockIncommingMessage();

        expect(instance.state).toEqual('uninitialised');

        instance.handleResponse(mockIncommingResponse);

        instance.on('closed', () => {
          expect(instance.state).toEqual('dead');
          done();
        });

        mockIncommingResponse.emit('close');
      });

      test('should set state to active when data is received', () => {
        const instance: TestInstance = new eventSourceListnerTS.EventSourceListner('http://example.com');
        const mockIncommingResponse = new MockIncommingMessage();

        expect(instance.state).toEqual('uninitialised');

        instance.handleResponse(mockIncommingResponse);

        mockIncommingResponse.emit('data', Buffer.from(':Connected'));

        expect(instance.state).toEqual('active');
      });

      test('should emit heartbeat event when heartbeat is received', (done) => {
        const instance: TestInstance = new eventSourceListnerTS.EventSourceListner('http://example.com');
        const mockIncommingResponse = new MockIncommingMessage();

        expect(instance.state).toEqual('uninitialised');

        instance.on('heartbeat', () => {
          done();
        });

        instance.handleResponse(mockIncommingResponse);

        mockIncommingResponse.emit('data', Buffer.from(':Heartbeat'));
      });

      test('should emit connected event when connected', (done) => {
        const instance: TestInstance = new eventSourceListnerTS.EventSourceListner('http://example.com');
        const mockIncommingResponse = new MockIncommingMessage();

        expect(instance.state).toEqual('uninitialised');

        instance.on('connected', () => {
          done();
        });

        instance.handleResponse(mockIncommingResponse);

        mockIncommingResponse.emit('data', Buffer.from(':Connected'));
      });

      test('should emit error event when error message is received', (done) => {
        const instance: TestInstance = new eventSourceListnerTS.EventSourceListner('http://example.com');
        const mockIncommingResponse = new MockIncommingMessage();

        expect(instance.state).toEqual('uninitialised');

        instance.on('error', (err: any) => {
          expect(err).toBeInstanceOf(Error);
          expect(err.message).toContain('test');
          done();
        });

        instance.handleResponse(mockIncommingResponse);

        mockIncommingResponse.emit('data', Buffer.from(JSON.stringify({
          RESPONSE: {
            RESULT: [{
              ERROR: { MESSAGE: 'test', SOURCE: 'test' }
            }]
          }
        })));
      });

      test('should emit message event when data is received', (done) => {
        const instance: TestInstance = new eventSourceListnerTS.EventSourceListner('http://example.com');
        const mockIncommingResponse = new MockIncommingMessage();

        expect(instance.state).toEqual('uninitialised');

        instance.on('message', (message: any) => {
          expect(message).toBeObject();
          expect(message).toEqual({ test: 'test' });
          done();
        });

        instance.handleResponse(mockIncommingResponse);

        const mockResponse = {
          RESPONSE: {
            RESULT: [{
              test: 'test'
            }]
          }
        };

        mockIncommingResponse.emit('data', Buffer.from(`
          id: 12345
          data: ${JSON.stringify(mockResponse)}`));
      });
    });

    describe('close', () => {
      test('sets state to dead when reocnnect is false', () => {
        const instance: TestInstance = new eventSourceListnerTS.EventSourceListner('http://example.com', false);

        instance.close(true);

        expect(instance.state).toEqual('dead');
      });

      test('emits closed event when reocnnect is false', (done) => {
        const instance: TestInstance = new eventSourceListnerTS.EventSourceListner('http://example.com', false);

        instance.on('closed', () => {
          done();
        });

        instance.close(true);
      });
    });

    describe('on', () => {
      test('returns this', () => {
        const instance: TestInstance = new eventSourceListnerTS.EventSourceListner('http://example.com');

        const result = instance.on('test', () => {});

        expect(result).toEqual(instance);
      });
    });

    afterEach(() => {
      jest.clearAllTimers();
    });
  });
});
