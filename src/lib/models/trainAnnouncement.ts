import { Response } from './response';

import { CodeDescription, RawCodeDescription, parseCodeDescription} from '../helpers/codeDescription';
import { Location, RawLocation, parseLocation } from '../helpers/location';
import { SchemaSwitch, isActiveSchema } from '../helpers/schemaSwitch';

export enum TASchemaVersion {
  'v1' = '1',
  'v1Dot1' = '1.1',
  'v1Dot3' = '1.3',
  'v1Dot4' = '1.4',
  'v1Dot5' = '1.5',
  'v1Dot6' = '1.6',
}
export type TASchemaVersions = TASchemaVersion | '1' | '1.1' | '1.3' | '1.4' | '1.5' | '1.6';
export type DefaultSchemaVersion = '1.6';
export const defaultSchemaVersion: TASchemaVersions & DefaultSchemaVersion = '1.6';

type AboveV1Dot6 = TASchemaVersion.v1Dot6;
type AboveV1Dot5 = TASchemaVersion.v1Dot5 | AboveV1Dot6;
type AboveV1Dot4 = TASchemaVersion.v1Dot4 | AboveV1Dot5;
type AboveV1Dot3 = TASchemaVersion.v1Dot3 | AboveV1Dot4;
type AboveV1Dot1 = TASchemaVersion.v1Dot1 | AboveV1Dot3;

const aboveV1Dot6: TASchemaVersion[] = [TASchemaVersion.v1Dot6];
const aboveV1Dot5: TASchemaVersion[] = [TASchemaVersion.v1Dot5, ...aboveV1Dot6];
const aboveV1Dot4: TASchemaVersion[] = [TASchemaVersion.v1Dot4, ...aboveV1Dot5];
const aboveV1Dot3: TASchemaVersion[] = [TASchemaVersion.v1Dot3, ...aboveV1Dot4];
const aboveV1Dot1: TASchemaVersion[] = [TASchemaVersion.v1Dot1, ...aboveV1Dot3];

type TASchemaSwitch<
  ActiveSchema extends TASchemaVersions,
  CurrentSchema extends TASchemaVersions,
  ActiveType,
  DefaultType
> = SchemaSwitch<TASchemaVersions, ActiveSchema, CurrentSchema, ActiveType, DefaultType>;

type Booking<T extends TASchemaVersions, U = CodeDescription> = TASchemaSwitch<AboveV1Dot5, T, U[], string[]>;
type Deviation<T extends TASchemaVersions, U = CodeDescription> = TASchemaSwitch<AboveV1Dot5, T, U[], string[]>;
type OtherInformation<T extends TASchemaVersions, U = CodeDescription> = TASchemaSwitch<AboveV1Dot5, T, U[], string[]>;
type ProductInformation<T extends TASchemaVersions, U = CodeDescription> = TASchemaSwitch<AboveV1Dot5, T, U[], string[]>;
type Service<T extends TASchemaVersions, U = CodeDescription> = TASchemaSwitch<AboveV1Dot5, T, U[], string[]>;
type TrainComposition<T extends TASchemaVersions, U = CodeDescription> = TASchemaSwitch<AboveV1Dot5, T, U[], string[]>;
type TypeOfTraffic<T extends TASchemaVersions, U = CodeDescription> = TASchemaSwitch<AboveV1Dot6, T, U[], string>;

type FromLocation<T extends TASchemaVersions, U = Location> = TASchemaSwitch<AboveV1Dot1, T, U[], string[]>;
type ToLocation<T extends TASchemaVersions, U = Location> = TASchemaSwitch<AboveV1Dot1, T, U[], string[]>;
type ViaFromLocation<T extends TASchemaVersions, U = Location> = TASchemaSwitch<AboveV1Dot1, T, U[], never>;
type ViaToLocation<T extends TASchemaVersions, U = Location> = TASchemaSwitch<AboveV1Dot1, T, U[], never>;

type NewEquipment<T extends TASchemaVersions> = TASchemaSwitch<AboveV1Dot1, T, number, never>;
type TechnicalTrainIdentity<T extends TASchemaVersions> = TASchemaSwitch<AboveV1Dot1, T, string, never>;
type Operator<T extends TASchemaVersions> = TASchemaSwitch<AboveV1Dot4, T, string, never>;
type PlannedEstimatedTimeAtLocationIsValid<T extends TASchemaVersions> = TASchemaSwitch<AboveV1Dot1, T, boolean, never>;
type TrainOwner<T extends TASchemaVersions> = TASchemaSwitch<AboveV1Dot4, T, string, never>;
type WebLinkName<T extends TASchemaVersions> = TASchemaSwitch<AboveV1Dot3, T, string, never>;

type TechnicalDateTime<T extends TASchemaVersions, U = Date> = TASchemaSwitch<AboveV1Dot4, T, U, never>;
type TimeAtLocationWithSeconds<T extends TASchemaVersions, U = Date> = TASchemaSwitch<AboveV1Dot4, T, U, never>;
type PlannedEstimatedTimeAtLocation<T extends TASchemaVersions, U = Date> = TASchemaSwitch<AboveV1Dot1, T, U, never>;

export interface RawTrainAnnouncementPayload<T extends TASchemaVersions> {
  ActivityId: string;
  ActivityType: string;
  Advertised: boolean;
  AdvertisedTimeAtLocation: string;
  AdvertisedTrainIdent: string;
  Booking: Booking<T, RawCodeDescription>;
  Cancled: boolean;
  Deleted: boolean;
  Deviation: Deviation<T, RawCodeDescription>;
  EstimatedTimeAtLocation: string;
  EstimatedTimeIsPreliminary: boolean;
  FromLocation: FromLocation<T, RawLocation>;
  LocationSignature: string;
  MobileWebLink: string;
  ModifiedTime: string;
  NewEquipment: NewEquipment<T>;
  Operator: Operator<T>;
  OtherInformation: OtherInformation<T, RawCodeDescription>;
  PlannedEstimatedTimeAtLocation: PlannedEstimatedTimeAtLocation<T, string>;
  PlannedEstimatedTimeAtLocationIsValid: PlannedEstimatedTimeAtLocationIsValid<T>;
  ProductInformation: ProductInformation<T, RawCodeDescription>;
  ScheduledDepartureDateTime: string;
  Service: Service<T, RawCodeDescription>;
  TechnicalDateTime: TechnicalDateTime<T, string>;
  TechnicalTrainIdent: TechnicalTrainIdentity<T>;
  TimeAtLocation: string;
  TimeAtLocationWithSeconds: TimeAtLocationWithSeconds<T, string>;
  ToLocation: ToLocation<T, RawLocation>;
  TrackAtLocation: string;
  TrainComposition: TrainComposition<T, RawCodeDescription>;
  TrainOwner: TrainOwner<T>;
  TypeOfTraffic: TypeOfTraffic<T, RawCodeDescription>;
  WebLink: string;
  WebLinkName: WebLinkName<T>;
  ViaFromLocation: ViaFromLocation<T, RawLocation>;
  ViaToLocation: ViaToLocation<T, RawLocation>;
}

const stringMap: (input: string) => string = (input: string) => input;

export class TrainAnnouncement<T extends TASchemaVersions> extends Response<T> {
  public activityId: string;
  public activityType: string;
  public advertised: boolean;
  public advertisedTimeAtLocation: Date;
  public advertisedTrainIdentity: string;
  public booking: Booking<T>;
  public cancled: boolean;
  public deleted: boolean;
  public deviation: Deviation<T>;
  public estimtedTimeAtLocation: Date;
  public estimatedTimeIsPreliminary: boolean;
  public fromLocation: FromLocation<T>;
  public informationOwner: string;
  public locationSignature: string;
  public mobileWebLink: string;
  public modifiedTime: Date;
  public newEquipment?: NewEquipment<T>;
  public operator?: Operator<T>;
  public otherInformation: OtherInformation<T>;
  public plannedEstimatedTimeAtLocation?: PlannedEstimatedTimeAtLocation<T>;
  public plannedEstimatedTimeAtLocationIsValid?: PlannedEstimatedTimeAtLocationIsValid<T>;
  public productInformation: ProductInformation<T>;
  public scheduledDepartureDateTime: Date;
  public service: Service<T>;
  public technicalDateTime?: TechnicalDateTime<T>;
  public technicalTrainIdentity?: TechnicalTrainIdentity<T>;
  public timeAtLocation: Date;
  public timeAtLocationWithSeconds?: TimeAtLocationWithSeconds<T>;
  public toLocation: ToLocation<T>;
  public trackAtLocation: string;
  public trainComposition: TrainComposition<T>;
  public trainOwner?: TrainOwner<T>;
  public typeOfTraffic: TypeOfTraffic<T>;
  public webLink: string;
  public webLinkName?: WebLinkName<T>;
  public viaFromLocation?: ViaFromLocation<T>;
  public viaToLocation?: ViaToLocation<T>;

  constructor(payload: RawTrainAnnouncementPayload<T>, schema: T) {
    super(schema);

    this.activityId = payload.ActivityId;
    this.activityType = payload.ActivityType;
    this.advertised = payload.Advertised;
    this.advertisedTimeAtLocation = TrainAnnouncement.parseDate(payload.AdvertisedTimeAtLocation);
    this.advertisedTrainIdentity = payload.AdvertisedTrainIdent;
    this.booking = (isActiveSchema(aboveV1Dot5, schema)
      ? ((payload.Booking || []) as RawCodeDescription[]).map(parseCodeDescription)
      : ((payload.Booking || []) as string[]).map(stringMap)) as Booking<T>;
    this.cancled = payload.Cancled || false;
    this.deleted = payload.Deleted || false;
    this.deviation = (isActiveSchema(aboveV1Dot5, schema)
      ? ((payload.Deviation || []) as RawCodeDescription[]).map(parseCodeDescription)
      : ((payload.Deviation || []) as string[]).map(stringMap)) as Deviation<T>;
    this.estimtedTimeAtLocation = TrainAnnouncement.parseDate(payload.EstimatedTimeAtLocation);
    this.estimatedTimeIsPreliminary = payload.EstimatedTimeIsPreliminary;
    this.fromLocation = (isActiveSchema(aboveV1Dot1, schema)
      ? ((payload.FromLocation || []) as RawLocation[]).map(parseLocation)
      : ((payload.FromLocation || []) as string[]).map(stringMap)) as FromLocation<T>;
    this.locationSignature = payload.LocationSignature;
    this.mobileWebLink = payload.MobileWebLink;
    this.modifiedTime = TrainAnnouncement.parseDate(payload.ModifiedTime);
    this.otherInformation = (isActiveSchema(aboveV1Dot5, schema)
      ? ((payload.OtherInformation || []) as RawCodeDescription[]).map(parseCodeDescription)
      : ((payload.OtherInformation || []) as string[]).map(stringMap)) as OtherInformation<T>;
    this.productInformation = (isActiveSchema(aboveV1Dot5, schema)
      ? ((payload.ProductInformation || []) as RawCodeDescription[]).map(parseCodeDescription)
      : ((payload.ProductInformation || []) as string[]).map(stringMap)) as ProductInformation<T>;

    this.scheduledDepartureDateTime = TrainAnnouncement.parseDate(payload.ScheduledDepartureDateTime);
    this.service = (isActiveSchema(aboveV1Dot5, schema)
      ? ((payload.Service || []) as RawCodeDescription[]).map(parseCodeDescription)
      : ((payload.Service || []) as string[]).map(stringMap)) as Service<T>;
    this.timeAtLocation = TrainAnnouncement.parseDate(payload.TimeAtLocation);
    this.toLocation = (isActiveSchema(aboveV1Dot1, schema)
      ? ((payload.ToLocation || []) as RawLocation[]).map(parseLocation)
      : ((payload.ToLocation || []) as string[]).map(stringMap)) as ToLocation<T>;
    this.trackAtLocation = payload.TrackAtLocation;
    this.trainComposition = (isActiveSchema(aboveV1Dot5, schema)
      ? ((payload.TrainComposition || []) as RawCodeDescription[]).map(parseCodeDescription)
      : ((payload.TrainComposition || []) as string[]).map(stringMap)) as TrainComposition<T>;
    this.typeOfTraffic = (isActiveSchema(aboveV1Dot6, schema)
      ? ((payload.TypeOfTraffic || []) as RawCodeDescription[]).map(parseCodeDescription)
      : payload.TypeOfTraffic as string) as TypeOfTraffic<T>;
    this.webLink = payload.WebLink;


    if (isActiveSchema(aboveV1Dot1, schema)) {
      this.newEquipment = payload.NewEquipment;
      this.technicalTrainIdentity = payload.TechnicalTrainIdent;

      this.plannedEstimatedTimeAtLocation = TrainAnnouncement.parseDate(
        payload.PlannedEstimatedTimeAtLocation,
      ) as PlannedEstimatedTimeAtLocation<T>;

      this.plannedEstimatedTimeAtLocationIsValid = payload.PlannedEstimatedTimeAtLocationIsValid;
      this.viaFromLocation = (((payload.ViaFromLocation || []) as RawLocation[]).map(parseLocation)) as ViaFromLocation<T>;
      this.viaToLocation = (((payload.ViaToLocation || []) as RawLocation[]).map(parseLocation)) as ViaToLocation<T>;
    }

    if (isActiveSchema(aboveV1Dot3, schema)) {
      this.webLinkName = payload.WebLinkName;
    }

    if (isActiveSchema(aboveV1Dot4, schema)) {
      this.operator = payload.Operator;
      this.technicalDateTime =
        TrainAnnouncement.parseDate(payload.TechnicalDateTime) as TechnicalDateTime<T>;
      this.timeAtLocationWithSeconds =
        TrainAnnouncement.parseDate(payload.TimeAtLocationWithSeconds) as TimeAtLocationWithSeconds<T>;
      this.trainOwner = payload.TrainOwner;
    }
  }
}
