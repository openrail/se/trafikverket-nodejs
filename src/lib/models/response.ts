interface RawError {
  SOURCE: string;
  MESSAGE: string;
}

interface RawInfo {
  SSEURL?: string;
  LASTCHANGEID?: string;
}

export interface RawResult {
  ERROR?: RawError;
  INFO?: RawInfo;
}

export interface RawResponse {
  RESPONSE?: {
    RESULT?: RawResult[],
  };
}

export function isErrorResponse(response: RawResult): boolean {
  return !!(response && response.ERROR);
}

export function getFirstErrorResponse(response: RawResponse): RawResult {
  return (response && response.RESPONSE && response.RESPONSE.RESULT
    && Array.isArray(response.RESPONSE.RESULT)
    && response.RESPONSE.RESULT.find((result) => isErrorResponse(result))) || null;
}

export function hasErrorResponse(response: RawResponse): boolean {
  return !!getFirstErrorResponse(response);
}

export class Response<T> {
  public schema: T;

  constructor(schema: T) {
    this.schema = schema;
  }

  protected static parseDate(date: string): Date | null {
    return (date) ? new Date(date) : null;
  }
}
