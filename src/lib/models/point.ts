export enum PointType {
  SWEREF99TM = 'SWEREF99TM',
  WGS84 = 'WGS84',
}
export type PointTypes = PointType | keyof typeof PointType;

const pointRegex = /POINT\s+\(([0-9\.]+?)\s+([0-9\.]+?)\)/i;

export class Point<T extends PointTypes> {
  private type: T;
  private axis1: number;
  private axis2: number;

  constructor(type: T, rawPoint: string);
  constructor(type: PointType.WGS84, latitude: number, longitude: number);
  constructor(type: PointType.SWEREF99TM, easting: number, northing: number);
  constructor(type: T, axis1: number | string, axis2?: number) {
    this.type = type;

    if (typeof axis1 === 'number') {
      this.axis1 = axis1;
      this.axis2 = axis2;
    } else if (pointRegex.test(axis1)) {
      const matches = axis1.match(pointRegex);

      this.axis1 = parseFloat(matches[1]);
      this.axis2 = parseFloat(matches[2]);
    } else {
      throw new Error('Unable to parse point data. needs to be either 2 arguments with numbers or string in the format of "POINT (n n)"');
    }
  }

  get latitude(): number {
    return this.axis1;
  }

  get longitude(): number {
    return this.axis2;
  }

  get easting(): number {
    return this.axis1;
  }

  get northing(): number {
    return this.axis2;
  }

  public toJSON(includeType: boolean = false): object {
    const json = (this.type === 'SWEREF99TM')
      ? { easting: this.easting, northing: this.northing }
      : { latitude: this.latitude, longitude: this.longitude };

    return (includeType)
      ? { type: this.type, ...json }
      : json;
  }

  public toString(includeType: boolean = false): string {
    return (includeType)
      ? `${this.type} (${this.axis1} ${this.axis2})`
      : `POINT (${this.axis1} ${this.axis2})`;
  }

  public static SWEREF99TM(rawPoint: string): Point<PointType.SWEREF99TM>;
  public static SWEREF99TM(easting: number, northing: number): Point<PointType.SWEREF99TM>;
  public static SWEREF99TM(axis1: number | string, axis2?: number): Point<PointType.SWEREF99TM> {
    if (typeof axis1 === 'number') {
      return new Point<PointType.SWEREF99TM>(PointType.SWEREF99TM, axis1, axis2);
    }

    return new Point<PointType.SWEREF99TM>(PointType.SWEREF99TM, axis1);
  }

  public static WGS84(rawPoint: string): Point<PointType.WGS84>;
  public static WGS84(latitude: number, longitude: number): Point<PointType.WGS84>;
  public static WGS84(axis1: number | string, axis2?: number): Point<PointType.WGS84> {
    if (typeof axis1 === 'number') {
      return new Point<PointType.WGS84>(PointType.WGS84, axis1, axis2);
    }

    return new Point<PointType.WGS84>(PointType.WGS84, axis1);
  }
}
