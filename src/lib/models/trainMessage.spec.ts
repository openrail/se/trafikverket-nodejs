
import 'jest';
import 'jest-extended';
import * as trainMessageTS from './trainMessage';

import { Point } from './point';

type TestInstance = any;

const mockdataV1: TestInstance = {
  AffectedLocation: ['test'],
  CountyNo: [1],
  Deleted: false,
  EventId: 'test',
  ExternalDescription: 'test',
  Geometry: {
    SWEREF99TM: 'POINT (0 0)',
    WGS84: 'POINT (0 0)'
  },
  LastUpdateDateTime: new Date().toISOString(),
  ModifiedTime: new Date().toISOString(),
  ReasonCodeText: 'test',
  StartDateTime: new Date().toISOString(),
};

const mockdataV1Dot3: TestInstance = Object.assign({}, mockdataV1, {
  EndDateTime: new Date().toISOString(),
  Header: 'test',
  TrafficImpact: [{
    AffectedLocation: ['test'],
    FromLocation: ['test'],
    ToLocation: ['test']
  }]
});

const mockdataV1Dot4: TestInstance = Object.assign({}, mockdataV1Dot3, {
  ExpectTrafficImpact: false,
  PrognosticatedEndDateTimeTrafficImpact: new Date().toISOString()
});

const mockdataV1Dot5: TestInstance = Object.assign({}, mockdataV1Dot4, {
  ReasonCodeText: undefined,
  ReasonCode: [{ Code: 'test', Description: 'test' }]
});

function instanceDatacheckV1(
  mockdata: TestInstance,
  schema: trainMessageTS.TMSchemaVersions,
  activeSchema: boolean
): void {
  test('should set schema on creation', () => {
    const instance: TestInstance = new trainMessageTS.TrainMessage(mockdata, schema);

    expect(instance.schema).toEqual(schema);
  });

  test('should set advertised on creation', () => {
    const instance: TestInstance = new trainMessageTS.TrainMessage(mockdata, schema);

    expect(instance.affectedLocation).toBeArray();
    expect(instance.affectedLocation).toEqual(mockdata.AffectedLocation);
  });

  test('should set counties on creation', () => {
    const instance: TestInstance = new trainMessageTS.TrainMessage(mockdata, schema);

    expect(instance.counties).toBeArray();
    expect(instance.counties).toEqual(mockdata.CountyNo);
  });

  test('should set deleted on creation', () => {
    const instance: TestInstance = new trainMessageTS.TrainMessage(mockdata, schema);

    expect(instance.deleted).toBeBoolean();
    expect(instance.deleted).toEqual(mockdata.Deleted);
  });

  test('should set eventId on creation', () => {
    const instance: TestInstance = new trainMessageTS.TrainMessage(mockdata, schema);

    expect(instance.eventId).toBeString();
    expect(instance.eventId).toEqual(mockdata.EventId);
  });

  test('should set externalDescription on creation', () => {
    const instance: TestInstance = new trainMessageTS.TrainMessage(mockdata, schema);

    expect(instance.externalDescription).toBeString();
    expect(instance.externalDescription).toEqual(mockdata.ExternalDescription);
  });

  test('should set externalDescription on creation', () => {
    const instance: TestInstance = new trainMessageTS.TrainMessage(mockdata, schema);

    expect(instance.externalDescription).toBeString();
    expect(instance.externalDescription).toEqual(mockdata.ExternalDescription);
  });

  test('should set geometry on creation', () => {
    const instance: TestInstance = new trainMessageTS.TrainMessage(mockdata, schema);

    expect(instance.geometry).toBeObject();
    expect(instance.geometry).toContainAllKeys(['SWEREF99TM', 'WGS84']);
  });

  test('should set geometry.SWEREF99TM on creation', () => {
    const instance: TestInstance = new trainMessageTS.TrainMessage(mockdata, schema);

    expect(instance.geometry.SWEREF99TM).toBeInstanceOf(Point);
    expect(instance.geometry.SWEREF99TM).toEqual(Point.SWEREF99TM(mockdata.Geometry.SWEREF99TM));
  });

  test('should set geometry.WGS84 on creation', () => {
    const instance: TestInstance = new trainMessageTS.TrainMessage(mockdata, schema);

    expect(instance.geometry.WGS84).toBeInstanceOf(Point);
    expect(instance.geometry.WGS84).toEqual(Point.WGS84(mockdata.Geometry.WGS84));
  });

  test('should set lastUpdate on creation', () => {
    const instance: TestInstance = new trainMessageTS.TrainMessage(mockdata, schema);

    expect(instance.lastUpdate).toBeInstanceOf(Date);
    expect(instance.lastUpdate).toEqual(new Date(mockdata.LastUpdateDateTime));
  });

  test('should set modified on creation', () => {
    const instance: TestInstance = new trainMessageTS.TrainMessage(mockdata, schema);

    expect(instance.modified).toBeInstanceOf(Date);
    expect(instance.modified).toEqual(new Date(mockdata.ModifiedTime));
  });

  test('should set startDate on creation', () => {
    const instance: TestInstance = new trainMessageTS.TrainMessage(mockdata, schema);

    expect(instance.startDate).toBeInstanceOf(Date);
    expect(instance.startDate).toEqual(new Date(mockdata.StartDateTime));
  });
}

function instanceDatacheckV1Dot3(
  mockdata: TestInstance,
  schema: trainMessageTS.TMSchemaVersions,
  aboveSchema: boolean
): void {
  test('should set endDate on creation', () => {
    const instance: TestInstance = new trainMessageTS.TrainMessage(mockdata, schema);

    if (aboveSchema) {
      expect(instance.endDate).toBeInstanceOf(Date);
      expect(instance.endDate).toEqual(new Date(mockdata.EndDateTime));
    } else {
      expect(instance.endDate).toBeUndefined();
    }
  });

  test('should set header on creation', () => {
    const instance: TestInstance = new trainMessageTS.TrainMessage(mockdata, schema);

    if (aboveSchema) {
      expect(instance.header).toBeString();
      expect(instance.header).toEqual(mockdata.Header);
    } else {
      expect(instance.header).toBeUndefined();
    }
  });

  test('should set trafficImpact on creation', () => {
    const instance: TestInstance = new trainMessageTS.TrainMessage(mockdata, schema);

    if (aboveSchema) {
      expect(instance.trafficImpact).toBeArray();
      expect(instance.trafficImpact).toSatisfyAll((impact) => {
        return (impact
          && impact.affectedLocation && Array.isArray(impact.affectedLocation)
          && impact.toLocation && Array.isArray(impact.toLocation)
          && impact.fromLocation && Array.isArray(impact.fromLocation));
      });
    } else {
      expect(instance.trafficImpact).toBeUndefined();
    }
  });
}

function instanceDatacheckV1Dot4(
  mockdata: TestInstance,
  schema: trainMessageTS.TMSchemaVersions,
  aboveSchema: boolean
): void {
  test('should set expectTrafficImpact on creation', () => {
    const instance: TestInstance = new trainMessageTS.TrainMessage(mockdata, schema);

    if (aboveSchema) {
      expect(instance.expectTrafficImpact).toBeBoolean();
      expect(instance.expectTrafficImpact).toEqual(mockdata.ExpectTrafficImpact);
    } else {
      expect(instance.expectTrafficImpact).toBeUndefined();
    }
  });

  test('should set prognosticatedEndDateTimeTrafficImpact on creation', () => {
    const instance: TestInstance = new trainMessageTS.TrainMessage(mockdata, schema);

    if (aboveSchema) {
      expect(instance.prognosticatedEndDateTimeTrafficImpact).toBeInstanceOf(Date);
      expect(instance.prognosticatedEndDateTimeTrafficImpact).toEqual(new Date(mockdata.PrognosticatedEndDateTimeTrafficImpact));
    } else {
      expect(instance.prognosticatedEndDateTimeTrafficImpact).toBeUndefined();
    }
  });
}

function instanceDatacheckV1Dot5(
  mockdata: TestInstance,
  schema: trainMessageTS.TMSchemaVersions,
  aboveSchema: boolean
): void {
  test('should set reasonCodeText on creation', () => {
    const instance: TestInstance = new trainMessageTS.TrainMessage(mockdata, schema);

    if (aboveSchema) {
      expect(instance.reasonCodeText).toBeUndefined();
    } else {
      expect(instance.reasonCodeText).toBeString();
      expect(instance.reasonCodeText).toEqual(mockdata.ReasonCodeText);
    }
  });

  test('should set reasonCode on creation', () => {
    const instance: TestInstance = new trainMessageTS.TrainMessage(mockdata, schema);

    if (aboveSchema) {
      expect(instance.reasonCode).toBeArray();
      expect(instance.reasonCode).toEqual([{ code: 'test', description: 'test' }]);
    } else {
      expect(instance.reasonCode).toBeUndefined();
    }
  });
}

describe('trainMessage.ts', () => {
  describe('exports', () => {
    test('defaultSchemaVersion', () => {
      expect(trainMessageTS.defaultSchemaVersion).toBeDefined();
    });

    test('TrainStation', () => {
      expect(trainMessageTS.TrainMessage).toBeDefined();
    });
  });

  describe('defaultSchemaVersion', () => {
    test('default schema should be 1.5', () => {
      expect(trainMessageTS.defaultSchemaVersion).toEqual('1.5');
    });
  });

  describe('TrainMessage', () => {
    describe('constructor', () => {
      describe('schema 1', () => {
        const schema: trainMessageTS.TMSchemaVersions = '1';
        const mockdata: TestInstance = mockdataV1;

        instanceDatacheckV1(mockdata, schema, true);
        instanceDatacheckV1Dot3(mockdata, schema, false);
        instanceDatacheckV1Dot4(mockdata, schema, false);
        instanceDatacheckV1Dot5(mockdata, schema, false);
      });

      describe('schema 1.3', () => {
        const schema: trainMessageTS.TMSchemaVersions = '1.3';
        const mockdata: TestInstance = mockdataV1Dot3;

        instanceDatacheckV1(mockdata, schema, true);
        instanceDatacheckV1Dot3(mockdata, schema, true);
        instanceDatacheckV1Dot4(mockdata, schema, false);
        instanceDatacheckV1Dot5(mockdata, schema, false);
      });

      describe('schema 1.4', () => {
        const schema: trainMessageTS.TMSchemaVersions = '1.4';
        const mockdata: TestInstance = mockdataV1Dot4;

        instanceDatacheckV1(mockdata, schema, true);
        instanceDatacheckV1Dot3(mockdata, schema, true);
        instanceDatacheckV1Dot4(mockdata, schema, true);
        instanceDatacheckV1Dot5(mockdata, schema, false);
      });

      describe('schema 1.5', () => {
        const schema: trainMessageTS.TMSchemaVersions = '1.5';
        const mockdata: TestInstance = mockdataV1Dot5;

        instanceDatacheckV1(mockdata, schema, true);
        instanceDatacheckV1Dot3(mockdata, schema, true);
        instanceDatacheckV1Dot4(mockdata, schema, true);
        instanceDatacheckV1Dot5(mockdata, schema, true);
      });
    });
  });
});
