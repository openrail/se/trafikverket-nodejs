/**
 * @description TypeScript Single Shot Example
 * 
 * This method requests data once and that is the end of the flow, you will receive an array of data
 * containing the query result and can continue with the rest of your application. The example below
 * shows usign the TrainAnnouncement schema 1.5 api to find all announcements at the Lund (Lu) station.
 */

import {
  TrafikverketAPI,
  TrainAnnoucementQuery,
  TrainAnnouncement,
  Filter,
  DataField
} from '@openrailse/trafikverket';

// putting the api key into an environment variable is much prefered to hard coding it
const trafikverket: TrafikverketAPI = new TrafikverketAPI('someSuperSecretPrivateAPIKey');

const query: TrainAnnoucementQuery<'1.5'> = new TrainAnnoucementQuery<'1.5'>('1.5')
  .filter(Filter.equal(DataField.LocationSignature, 'Lu'))
  .once('message', (messageData: TrainAnnouncement<'1.5'>[]): void => {
    console.log('messagedata', messageData);
  })
  .once('error', (err: Error): void => {
    console.error(err);
  })
  .request(trafikverket);
