/**
 * @description JavaScript Single Shot Example
 * 
 * This method requests data once and that is the end of the flow, you will receive an array of data
 * containing the query result and can continue with the rest of your application. The example below
 * shows usign the TrainAnnouncement schema 1.5 api to find all announcements at the Lund (Lu) station.
 */

import {
  TrafikverketAPI,
  TrainAnnoucementQuery,
  Filter,
  DataField
} from '@openrailse/trafikverket';

// putting the api key into an environment variable is much prefered to hard coding it
const trafikverket = new TrafikverketAPI('someSuperSecretPrivateAPIKey');

const query = new TrainAnnoucementQuery('1.5')
  .filter(Filter.equal(DataField.LocationSignature, 'Lu'))
  .once('message', (messageData) => {
    console.log('messagedata', messageData);
  })
  .once('error', (err) => {
    console.error(err);
  })
  .request(trafikverket);