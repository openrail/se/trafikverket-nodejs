/**
 * @description JavaScript Streaming Example
 * 
 * This methoid works very similar to single shot except will continue to publish messages after the
 * first query is complete. This is done by enabling Server Side Events by passing in a true value
 * as a second parameter onto the query class and using .on() to subscribe to messages. The example
 * below shows usign the TrainAnnouncement schema 1.5 api to find all announcements at the Lund (Lu)
 * station.
 */

const {
  TrafikverketAPI,
  TrainAnnoucementQuery,
  Filter,
  DataField
} = require('@openrailse/trafikverket');

// putting the api key into an environment variable is much prefered to hard coding it
const trafikverket = new TrafikverketAPI('someSuperSecretPrivateAPIKey');

const query = new TrainAnnoucementQuery('1.5', true)
  .filter(Filter.equal(DataField.LocationSignature, 'Lu'))
  .on('message', (messageData) => {
    console.log('messagedata', messageData);
  })
  .on('close', () => {
    console.warn('Connection was closed. re-run the query again');
  })
  .on('error', (err) => {
    console.error(err);
  })
  .request(trafikverket);