module.exports = {
  testEnvironment: 'node',
  roots: [
    './src'
  ],
  testMatch: [
    '**/__tests__/**/*.+(ts|js)',
    '**/?(*.)+(spec|test).+(ts|js)'
  ],
  transform: {
    '^.+\\.ts$': 'ts-jest'
  },
  coverageDirectory: './artifacts/coverage',
  coverageReporters: [
    'text-summary',
    'lcov'
  ],
  coverageThreshold: {
    global: {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: 80
    }
  },
  collectCoverageFrom: [
    './src/**/*.{ts,js}'
  ],
  cacheDirectory: './cache',
  setupFilesAfterEnv: ['jest-extended']
};
