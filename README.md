# @openrailse/trafikverket

[![npm (scoped with tag)](https://img.shields.io/npm/v/@openrailse/trafikverket/latest)](https://www.npmjs.com/package/@openrailse/trafikverket)
[![License](https://img.shields.io/npm/l/@openrailse/trafikverket)](https://gitlab.com/openrail/se/trafikverket-nodejs/blob/master/LICENSE)
[![Pipeline Status](https://gitlab.com/openrail/se/trafikverket-nodejs/badges/master/pipeline.svg)](https://gitlab.com/openrail/se/trafikverket-nodejs)
[![Coverage Status](https://gitlab.com/openrail/se/trafikverket-nodejs/badges/master/coverage.svg)](https://gitlab.com/openrail/se/trafikverket-nodejs)
[![Discord](https://img.shields.io/discord/478848557089030144.svg)](https://discord.gg/N9CPKaY)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

API wrapper for [Trafikverket Öppet API](https://api.trafikinfo.trafikverket.se/) writen in TypeScript using built in Node.JS modules where possible to minimise dependencies. This package also aims to implement all available schema versions with correct typings so that future changes to the API wont break existing code.

## Table of Contents

* [Available Implementations](#available-implementations)
* [Instalation](#instalation)
* [Usage](#usage)

## Available Implementations

Currently this package only implements the following [Trafikverket Öppet API](https://api.trafikinfo.trafikverket.se/) endpoints:

* Railroad Infomation
  * `ReasonCode` Provides reason codes with descriptions as to what they mean
  * `TrainAnnouncement` Provides timetable information corresponding to specific trains
  * `TrainMessage` Traffic announcements including rail works and rail faults
  * `TrainStation` Information about stations

If you require any of the other implementations, raise an issue to have it implemented or raise an issue and PR with the implementation. To see a full list of what is available to be implemnted see the [Trafikverket Öppet API - API Model](https://api.trafikinfo.trafikverket.se/API/Model) page.

## Instalation

To install this package using npm run:
```
npm install @openrailse/trafikverket
```

or if yarn is more your thing:
```
yarn add @openrailse/trafikverket
```

## Usage

There are a couple of different options when using this package including single shot and streaming.

### Single Shot

This will send a one off query to Trafikverket Open API and return the queries results ending the whole transaction.
This is usefull if you need one off data.

### Streaming

This will first send an initial request to Trafikverket Open API like single shot but will then subscribe to Server Side Events and continously emit new updated data. 

### Examples

The [example folder](https://gitlab.com/openrail/se/trafikverket-nodejs/tree/master/example) within the repository contains code examples as well as [GitLab Snippets](https://gitlab.com/openrail/se/trafikverket-nodejs/snippets):
  * Single Shot
    * [CommonJS](https://gitlab.com/openrail/se/trafikverket-nodejs/snippets/1918426)
    * [ES6 modules](https://gitlab.com/openrail/se/trafikverket-nodejs/snippets/1918427)
    * [TypeScript](https://gitlab.com/openrail/se/trafikverket-nodejs/snippets/1918428)
  * Streaming
    * [CommonJS](https://gitlab.com/openrail/se/trafikverket-nodejs/snippets/1918429)
    * [ES6 modules](https://gitlab.com/openrail/se/trafikverket-nodejs/snippets/1918430)
    * [TypeScript](https://gitlab.com/openrail/se/trafikverket-nodejs/snippets/1918432)
